package test

import com.badlogic.gdx.backends.lwjgl.LwjglApplication
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration

class TestMain {
     companion object {
         @JvmStatic fun main(arg: Array<String>) {
            val config = LwjglApplicationConfiguration()
            LwjglApplication(TestGame(), config)
        }
    }

}