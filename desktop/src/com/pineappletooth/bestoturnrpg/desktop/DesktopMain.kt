package com.pineappletooth.bestoturnrpg.desktop


import com.badlogic.gdx.backends.lwjgl.LwjglApplication
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration
import com.pineappletooth.bestoturnrpg.BestoTurnRPG



class DesktopMain {
     companion object {
         @JvmStatic fun main(arg: Array<String>) {
            val config = LwjglApplicationConfiguration()
             //config.width = 1536
            // config.height = 864
             config.title = "NOSEDEQUETRATAESTEJUEGO"
             //config.fullscreen = true;
             config.width = LwjglApplicationConfiguration.getDesktopDisplayMode().width
             config.height = LwjglApplicationConfiguration.getDesktopDisplayMode().height-80
             //config.width = 1280;
            // config.height = 720;

             //config.fullscreen = true;

            LwjglApplication(BestoTurnRPG(), config)
        }
    }

}