package test

import com.badlogic.gdx.Gdx.graphics
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import java.awt.Rectangle


class Ball(var x: Int, var y: Int, var size: Int, var xSpeed: Int, var ySpeed: Int) {
    var color = Color.WHITE
    fun update() {
        x += xSpeed
        y += ySpeed
        if (x < size || x > graphics.width-size) {
            xSpeed = -xSpeed
        }
        if (y < size || y > graphics.height-size) {
            ySpeed = -ySpeed
        }
    }
    fun checkCollision(paddle: Palito){
        if(collidesWith(Rectangle(paddle.x,paddle.y,paddle.ancho,paddle.alto))){
            ySpeed = -ySpeed
        }
    }
    fun checkCollision(b: Bloque){
        if(collidesWith(Rectangle(b.x,b.y,b.ancho,b.alto))){
            ySpeed = -ySpeed
            b.destruido = true

        }
    }
    private fun collidesWith(rectangulo : Rectangle): Boolean {
        return rectangulo.intersects(x.toDouble()-size,y.toDouble()-size,size.toDouble()*2,size.toDouble()*2)
    }

    fun draw(shape: ShapeRenderer) {
        shape.color = color
        shape.circle(x.toFloat(), y.toFloat(), size.toFloat())


    }

}