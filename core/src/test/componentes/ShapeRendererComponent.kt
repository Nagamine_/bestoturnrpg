package test.componentes

import com.badlogic.ashley.core.Component
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.math.Circle
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.math.Shape2D


class ShapeRendererComponent( var tipo :ShapeRenderer.ShapeType = ShapeRenderer.ShapeType.Filled, var color: Color = Color.WHITE) : Component