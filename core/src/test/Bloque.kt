package test

import com.badlogic.gdx.graphics.glutils.ShapeRenderer


class Bloque(var x: Int, var y: Int, var ancho: Int, var alto: Int) {
    var destruido = false
    fun draw(shape: ShapeRenderer) {
        shape.rect(x.toFloat(), y.toFloat(), ancho.toFloat(), alto.toFloat())
    }

}