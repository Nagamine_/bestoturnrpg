package test

import com.badlogic.ashley.core.Engine
import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.core.PooledEngine
import com.badlogic.gdx.Gdx.graphics
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.math.Circle
import com.badlogic.gdx.math.Rectangle
import ktx.app.KtxApplicationAdapter
import ktx.app.clearScreen
import ktx.ashley.entity
import ktx.collections.gdxArrayOf
import test.componentes.*
import test.sistemas.*


class TestGame : KtxApplicationAdapter {
    lateinit var shape: ShapeRenderer
    lateinit var ball: Entity
    lateinit var palito: Entity
    lateinit var motor: Engine
    lateinit var sistemaMovimiento: SistemaDeMovimientoTest
    lateinit var sistemaDeDibujado: SistemaDeDibujado
    lateinit var sistemaDeColision: SistemaDeColision
    lateinit var sistemaDeControl: SistemaDeControl
    lateinit var sistemaDeDestruccion: SistemaDeDestruccion
    var lista = gdxArrayOf<Bloque>()
    override fun create() {
        motor = PooledEngine()
        sistemaMovimiento = SistemaDeMovimientoTest()
        sistemaDeDibujado = SistemaDeDibujado()
        sistemaDeColision = SistemaDeColision()
        sistemaDeControl = SistemaDeControl()
        sistemaDeDestruccion = SistemaDeDestruccion()

        motor.addSystem(sistemaMovimiento)
        motor.addSystem(sistemaDeDibujado)
        motor.addSystem(sistemaDeColision)
        motor.addSystem(sistemaDeControl)
        motor.addSystem(sistemaDeDestruccion)
        shape = ShapeRenderer()

        ball = motor.entity {
            with<PosicionComponentTest> {
                x = 400f
                y = 20f
            }
            with<VelocidadComponentTest> {
                speedX = 300f
                speedY = 240f
            }
            with<ShapeRendererComponent> { }
            with<BallComponent> { }
            with<FormaComponent> {
                forma = Circle(20f, 10f, 8f)
            }
        }

        palito = motor.entity {
            with<PosicionComponentTest> {
                x = 400f
                y = 20f
            }
            with<ShapeRendererComponent> { }
            with<FormaComponent> {
                forma = Rectangle(20f, 20f, 120f, 15f)
            }
            with<MouseControlComponent> {

            }
        }


        val blockWidth = 63
        val blockHeight = 20

        var y = graphics.height / 2
        while (y < graphics.height) {
            var x = 0
            while (x < graphics.width) {
                //bloque
                motor.entity {
                    with<PosicionComponentTest> {
                        this.x = x.toFloat()
                        this.y = y.toFloat()
                    }
                    with<ShapeRendererComponent> { }
                    with<FormaComponent> {
                        forma = Rectangle(x.toFloat(), y.toFloat(), blockWidth.toFloat(), blockHeight.toFloat())
                    }
                    with<DestruibleComponent>()
                }
                x += blockWidth + 10
            }
            y += blockHeight + 10
        }

        /* for (i in 0..9) {
             lista.add(Ball(MathUtils.random(graphics.width),
                     MathUtils.random(graphics.height),
                     MathUtils.random(100) , MathUtils.random(15), MathUtils.random(15)))
         }*/
    }

    override fun render() {
        clearScreen(0f, 0f, 0f, 0f)
        motor.update(graphics.deltaTime)


        //ball.checkCollision(palito)



    }
}