package test.sistemas

import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.systems.IteratingSystem
import com.badlogic.gdx.Gdx.input
import com.badlogic.gdx.math.Rectangle
import ktx.ashley.allOf
import ktx.ashley.mapperFor
import test.componentes.FormaComponent
import test.componentes.MouseControlComponent
import test.componentes.PosicionComponentTest

class SistemaDeControl : IteratingSystem(allOf(PosicionComponentTest::class, MouseControlComponent::class,FormaComponent::class).get()) {
    var posicion = mapperFor<PosicionComponentTest>()
    var forma = mapperFor<FormaComponent>()
    var control = mapperFor<MouseControlComponent>()
    override fun processEntity(entity: Entity?, deltaTime: Float) {
        var f = forma.get(entity).forma
        if(f is Rectangle) {

            var c = control.get(entity)
            c.posicionX = input.x
            c.posicionY = input.y
            posicion.get(entity).x = c.posicionX.toFloat() - f.width/2
        }
    }
}