package test.sistemas

import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.systems.IteratingSystem
import ktx.ashley.allOf
import ktx.ashley.mapperFor
import test.componentes.DestruibleComponent

class SistemaDeDestruccion : IteratingSystem(allOf(DestruibleComponent::class).get()) {
    var destruidos = mapperFor<DestruibleComponent>()
    override fun processEntity(entity: Entity?, deltaTime: Float) {
        if(destruidos.get(entity).destruido){
            engine.removeEntity(entity)
        }
    }

}