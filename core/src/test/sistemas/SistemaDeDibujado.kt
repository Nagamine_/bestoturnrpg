package test.sistemas

import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.systems.SortedIteratingSystem
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.math.Circle
import com.badlogic.gdx.math.Rectangle
import ktx.ashley.allOf
import ktx.ashley.mapperFor
import ktx.graphics.use
import test.componentes.FormaComponent
import test.componentes.PosicionComponentTest
import test.componentes.ShapeRendererComponent
import kotlin.math.sign


class SistemaDeDibujado : SortedIteratingSystem(allOf(ShapeRendererComponent::class).get(), ZComparator() ){
    var shape = mapperFor<ShapeRendererComponent>()
    var posicion = mapperFor<PosicionComponentTest>()
    var forma = mapperFor<FormaComponent>()
    var shapeRenderer = ShapeRenderer()
    override fun processEntity(entity: Entity, deltaTime: Float) {
       var figura = forma.get(entity).forma
        var pos = posicion.get(entity)
        shapeRenderer.use(shape.get(entity).tipo){

            shapeRenderer.color = shape.get(entity).color
            when(figura){
                is Circle -> shapeRenderer.circle(pos.x,pos.y,figura.radius)
                is Rectangle -> shapeRenderer.rect(pos.x,pos.y,figura.width,figura.height)
            }
        }

    }
    private class ZComparator : Comparator<Entity?> {
        private val pm = mapperFor<PosicionComponentTest>()
        override fun compare(e1: Entity?, e2: Entity?): Int {
            return sign(pm[e1].z - pm[e2].z).toInt()
        }
    }

}