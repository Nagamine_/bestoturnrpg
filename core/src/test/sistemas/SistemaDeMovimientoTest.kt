package test.sistemas

import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.systems.IteratingSystem
import ktx.ashley.allOf
import ktx.ashley.mapperFor
import test.componentes.PosicionComponentTest
import test.componentes.VelocidadComponentTest


class SistemaDeMovimientoTest : IteratingSystem(allOf(VelocidadComponentTest::class,PosicionComponentTest::class).get()) {

    var posicion = mapperFor<PosicionComponentTest>()
    var velocidad = mapperFor<VelocidadComponentTest>()
    override fun processEntity(entity: Entity, deltaTime: Float) {

        val position = posicion.get(entity)
        val velocity = velocidad.get(entity)
        position.x += velocity.speedX * deltaTime
        position.y += velocity.speedY * deltaTime
    }
}