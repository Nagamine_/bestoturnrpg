package test.sistemas

import com.badlogic.ashley.core.Engine
import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.systems.IteratingSystem
import com.badlogic.ashley.utils.ImmutableArray
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.math.Circle
import com.badlogic.gdx.math.Rectangle
import ktx.ashley.allOf
import ktx.ashley.exclude
import ktx.ashley.mapperFor
import test.componentes.*



class SistemaDeColision : IteratingSystem(allOf(FormaComponent::class,PosicionComponentTest::class,VelocidadComponentTest::class,BallComponent::class).get()) {
    private lateinit var entidades: ImmutableArray<Entity>
    private lateinit var destruidos: ImmutableArray<Entity>
    var bordes = mapperFor<FormaComponent>()
    var velocidad = mapperFor<VelocidadComponentTest>()
    var posicion = mapperFor<PosicionComponentTest>()
    var destruible = mapperFor<DestruibleComponent>()
    var ballHitBox =Rectangle()

    override fun addedToEngine(engine: Engine) {
        super.addedToEngine(engine)
        entidades = engine.getEntitiesFor(allOf(FormaComponent::class,PosicionComponentTest::class).exclude(BallComponent::class).get())
        destruidos = engine.getEntitiesFor(allOf(DestruibleComponent::class).get())
    }

    override fun processEntity(entity: Entity, deltaTime: Float) {
        val figura = bordes.get(entity).forma
        var hitBox = Rectangle()

        val velY = velocidad.get(entity)
        if(figura is Circle) {
            if (posicion.get(entity).x < figura.radius || posicion.get(entity).x > Gdx.graphics.width - figura.radius) {
                velY.speedX = -velY.speedX
            }
            if (posicion.get(entity).y < figura.radius || posicion.get(entity).y > Gdx.graphics.height - figura.radius) {
                velY.speedY = -velY.speedY
            }
        }
        for(a in entidades){
            val d = bordes.get(a).forma
            if(d is Rectangle) {
                 hitBox = Rectangle(posicion.get(a).x, posicion.get(a).y, d.width,d.height)
            }
            if(figura is Circle){
                ballHitBox = Rectangle(posicion.get(entity).x -figura.radius,posicion.get(entity).y-figura.radius, figura.radius*2,figura.radius*2)
            }


                if(hitBox.overlaps(ballHitBox)){
                    if(destruidos.contains(a,true)){
                        destruible.get(a).destruido = true
                    }
                    velY.speedY = -velY.speedY
                }


        }
    }
}