package com.pineappletooth.bestoturnrpg




import com.badlogic.ashley.core.Engine
import com.badlogic.ashley.core.PooledEngine
import com.badlogic.gdx.Application
import com.badlogic.gdx.Gdx.app
import com.badlogic.gdx.Gdx.graphics
import com.badlogic.gdx.Screen
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.utils.viewport.ExtendViewport
import com.badlogic.gdx.utils.viewport.FitViewport
import com.badlogic.gdx.utils.viewport.Viewport
import com.pineapple.physics.core.Mundo
import com.pineapple.physics.utils.crearMundo

import com.pineappletooth.bestoturnrpg.constantes.ALTO
import com.pineappletooth.bestoturnrpg.constantes.ANCHO
import com.pineappletooth.bestoturnrpg.listeners.InputInterfaz
import com.pineappletooth.bestoturnrpg.listeners.InputListener
import com.pineappletooth.bestoturnrpg.managers.Assets
import com.pineappletooth.bestoturnrpg.screens.ExampleScreen
import com.pineappletooth.bestoturnrpg.sistemas.*
import com.pineappletooth.bestoturnrpg.sistemas.ia.IaSystem
import ktx.app.KtxGame
import ktx.app.clearScreen

//TODO Bug: falta arreglar en physics al chocar el tile y es no solido, no se convierte en solido
//TODO añadir mas efectos o generalizar efectos (congelar)
//TODO mejorar la IA
//TODO generalizar creacion de personajes
//TODO generacion programatica de niveles
//TODO separar listener de ia
class BestoTurnRPG : KtxGame<Screen>() {
    lateinit var engine : Engine
    lateinit var viewport: Viewport
    lateinit var viewportInterfaz : Viewport
    lateinit var camera : OrthographicCamera
    lateinit var cameraInterfaz : OrthographicCamera
    lateinit var mundo : Mundo
    lateinit var assets: Assets
    lateinit var inputListener : InputInterfaz
    lateinit var ia : IaSystem


    override fun create() {
        app.logLevel = Application.LOG_DEBUG
    //    graphics.setFullscreenMode(graphics.displayMode)
        engine = PooledEngine(20,1000,40,1000)

        assets = Assets()

        mundo = Mundo(crearMundo((ANCHO/Mundo.Configuracion.TAMANIO_TILE).toInt()*5,(ALTO/Mundo.Configuracion.TAMANIO_TILE).toInt()))

        camera = OrthographicCamera()
        cameraInterfaz = OrthographicCamera()
        inputListener = InputListener()
        //camera.setToOrtho(false,  ALT0*graphics.width/graphics.height, ALT0)

        viewport = ExtendViewport( ALTO*graphics.width/graphics.height, ALTO,camera)
        viewportInterfaz = FitViewport(1280f,720f,cameraInterfaz)
        viewportInterfaz.apply()
        cameraInterfaz.position.set(cameraInterfaz.viewportWidth/2,cameraInterfaz.viewportHeight/2,0f)
        viewport.apply()
        camera.position.set(camera.viewportWidth/2,camera.viewportHeight/2,0f)
        ia = IaSystem(0)
        addSistemas()


        //    println(viewport.screenHeight )

        addScreen(ExampleScreen())

        // Changing current screen to the registered instance of the
        // ExampleScreen class:
        setScreen<ExampleScreen>()




    }
    override fun render() {
        clearScreen(0f,0f,0f)
        camera.update()
        cameraInterfaz.update()

        //mundo.step(1/60f, 6, 2);

        engine.update(graphics.deltaTime)
        currentScreen.render(graphics.deltaTime)

    }

    override fun resize(width: Int, height: Int) {
        //super.resize(width, height)
        viewport.update(width,height)
        viewportInterfaz.update(width,height)
    }
    override fun dispose() {

    }
    fun addSistemas (){
        //metodos listerners y de control
        engine.addSystem(ControlSystem(1))
        engine.addSystem(ia)
        engine.addSystem(ProyectilSystem(2))
        engine.addSystem(MeleeSystem(2))
        engine.addSystem(VenenoSystem())

        //generadores de entidades, simulacion de las fisicas
        engine.addSystem(FisicaSystem(3))

        //actualiza posicion
        engine.addSystem(PosicionSystem(4))


        //simulacion de datos extra


        engine.addSystem(DamageSystem(6))
        engine.addSystem(VenenoEffectSystem(6))
        //dependientes de la posicion
        engine.addSystem(CameraSystem(5))

        engine.addSystem(FondoSystem(10))
        engine.addSystem(VidaSystem(11))
        //Dibujado al final
        engine.addSystem(DebugRendererSystem(12))
        engine.addSystem(SpriteSystem(12))
        engine.addSystem(AnimacionSystem(12))
        engine.addSystem(DibujarEfectoSystem(12))




        //println(json.prettyPrint(adapter as Entity))
        // json.setElementType(Person::class.java, "numbers", PhoneNumber::class.java)
        //  json.setElementType(Entity::class.java, "components", Component::class.java)



        //engine.addSystem(GravedadSystem())
        //engine.addSystem(MovimientoSystem())
        //engine.addSystem(ColisionSystem())

        //keytool -genkey -v -keystore ~/.android/debug.keystore -storepass android -alias androiddebugkey -keypass android -keyalg RSA -keysize 2048 -validity 10000

    }
}