package com.pineappletooth.bestoturnrpg.utils

import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.core.PooledEngine
import com.google.gson.*
import java.lang.reflect.Type

fun serialize(entidad : Entity){
    class DateTimeSerializer : JsonSerializer<Entity> {
        override fun serialize(src: Entity, typeOfSrc: Type?, context: JsonSerializationContext?): JsonElement {
            val s = Gson()
            val a = JsonObject()
            val b = JsonArray()

            for(f in src.components){
                b.add(s.toJsonTree(f))
            }
            //  b.add(s.toJson(src.components))
            a.add("components",b)
            // a.addProperty("ca",src.components.toString())
            return a
        }
    }
    for(f in PooledEngine::class.java.declaredClasses){
        println(f)
    }
    val gsonBuilder = GsonBuilder()
    val gson = gsonBuilder.setPrettyPrinting().registerTypeAdapter(Class.forName("com.badlogic.ashley.core.PooledEngine\$PooledEntity"),DateTimeSerializer()).create()
    println(gson.toJson(entidad))
}