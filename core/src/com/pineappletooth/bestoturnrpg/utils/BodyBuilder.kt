package com.pineappletooth.bestoturnrpg.utils

import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.utils.Pool
import com.pineappletooth.bestoturnrpg.BestoTurnRPG
import com.pineappletooth.bestoturnrpg.constantes.Direccion
import com.pineappletooth.bestoturnrpg.managers.posicionMapper
import com.pineappletooth.bestoturnrpg.managers.proyectilMapper
import com.pineappletooth.bestoturnrpg.managers.tamanioMapper
import ktx.collections.gdxArrayOf
import ktx.math.vec2



//No hay forma de hacer un pool de FixtureBuilders
class BodyBuilder private constructor(): Pool.Poolable{
    override fun reset() {

    }
//    companion object {
//        private val bodyBuilder : Pool<BodyBuilder> = object : Pool<BodyBuilder>() {
//            override fun newObject(): BodyBuilder {
//                return BodyBuilder()
//            }
//        }
//        fun startBuild() : BodyBuilder{
//            return bodyBuilder.obtain()
//        }
//    }
//    //No olvidar poner los stat en reset
//    private val juego = Gdx.app.applicationListener as BestoTurnRPG
//    private var tipo = BodyDef.BodyType.DynamicBody
//    private var posicion : Vector2? = null
//    private var puedeRotar = false
//    private var data: Any? = null
//    private var bullet = false
//    private var velocidad = vec2(0f, 0f)
//    private var gravedad = 1f
//    private var fixtures = gdxArrayOf<FixtureBuilder>()
//    private val dispose = gdxArrayOf<Shape>()
//    enum class Forma {
//        RECTANGULO,
//        CIRCULO,
//        LINEA
//    }
//
//    fun posicion(x: Float, y: Float): BodyBuilder {
//        posicion = vec2(x, y)
//        return this
//    }
//
//    fun tipo(tipe: BodyDef.BodyType): BodyBuilder {
//        tipo = tipe
//        return this
//    }
//
//    fun puedeRotar(): BodyBuilder {
//        puedeRotar = true
//        return this
//    }
//
//
//    fun bullet(): BodyBuilder {
//        bullet = true
//        return this
//    }
//
//    fun velocidad(x: Float, y: Float): BodyBuilder {
//        velocidad = vec2(x, y)
//        return this
//    }
//
//    fun gravedad(gravedad : Float) : BodyBuilder{
//        this.gravedad = gravedad
//        return this
//    }
//
//    fun fixture(): FixtureBuilder {
//        return FixtureBuilder()
//    }
//
//    fun build(entidad:Entity ): Body {
//        if(proyectilMapper.has(entidad)){
//            velocidad = vec2(proyectilMapper[entidad].velocidadX, proyectilMapper[entidad].velocidadY)
//            if(posicionMapper.has(entidad)){
//                if(posicionMapper[entidad].direccion == Direccion.IZQUIERDA){
//                    velocidad.x = -velocidad.x
//                }
//            }
//        }
//        val bodyDef = BodyDef().apply {
//            type = tipo
//            linearVelocity.set(velocidad)
//            bullet = bullet
//            fixedRotation = !puedeRotar
//            gravityScale = gravedad
//        }
//        if(posicion != null){
//            bodyDef.position.set(posicion)
//        }else{
//            if(posicionMapper.has(entidad)){
//                bodyDef.position.set(posicionMapper[entidad].x, posicionMapper[entidad].y)
//            }
//        }
//        val body: Body = juego.mundo.createBody(bodyDef)
//        body.userData = entidad
//        for (fixture in fixtures) {
//            val f = FixtureDef().apply {
//                density = fixture.densidad
//                friction = fixture.friccion
//                isSensor = fixture.sensor
//            }
//            if(fixture.tamanioX==null){
//                if(tamanioMapper.has(entidad)) {
//                    fixture.tamanioX = tamanioMapper[entidad].tamanioX
//                }
//            }
//            if(fixture.tamanioY==null){
//                if(tamanioMapper.has(entidad)) {
//                    fixture.tamanioY = tamanioMapper[entidad].tamanioY
//                }
//            }
//            val tamX = fixture.tamanioX
//            val tamY = fixture.tamanioY
//            if(tamX!=null && tamY!=null) {
//                when (fixture.forma) {
//                    Forma.RECTANGULO -> {
//                        val shape = PolygonShape()
//                        shape.setAsBox(tamX/ 2, tamY / 2)
//                        dispose.add(shape)
//                        f.shape = shape
//                    }
//                    Forma.LINEA -> {
//                        val shape = EdgeShape()
//                        shape.set(-tamX / 2, tamX / 2, tamX / 2, tamX / 2)
//                        dispose.add(shape)
//                        f.shape = shape
//                    }
//                    Forma.CIRCULO -> {
//                        val shape = CircleShape()
//                        shape.radius = tamX
//                        dispose.add(shape)
//                        f.shape = shape
//                    }
//                }
//                body.createFixture(f)
//            }
//        }
//        for (d in dispose){
//            d.dispose()
//        }
//        bodyBuilder.free(this)
//        return body
//    }
//
//    inner class FixtureBuilder() {
//
//        var tamanioX : Float?= null
//        var tamanioY : Float?= null
//        var forma = Forma.RECTANGULO
//        var posicion = vec2(0f, 0f)
//        var densidad = 0.6f
//        var friccion = 0.6f
//        var sensor = false
//
//        fun rectangulo(ancho: Float? = null, alto: Float? = null): FixtureBuilder {
//            tamanioX = ancho
//            tamanioY = alto
//            forma = Forma.RECTANGULO
//            return this
//        }
//
//        fun circulo(radio: Float? = null): FixtureBuilder {
//            tamanioX = radio
//            forma = Forma.CIRCULO
//            return this
//        }
//
//        fun linea(ancho: Float? = null): FixtureBuilder {
//            tamanioX = ancho
//            forma = Forma.LINEA
//            return this
//        }
//
//
//        fun posicion(x: Float, y: Float): FixtureBuilder {
//            posicion = vec2(x, y)
//            return this
//        }
//
//        fun densidad(densidad: Float): FixtureBuilder {
//            this.densidad = densidad
//            return this
//        }
//
//        fun friccion(friccion: Float): FixtureBuilder {
//            this.friccion = friccion
//            return this
//        }
//
//        fun sensor(): FixtureBuilder {
//            sensor = true
//            return this
//        }
//
//        fun buildFixture(): BodyBuilder {
//            fixtures.add(this)
//            return this@BodyBuilder
//        }
//    }
//
//    override fun reset() {
//        tipo = BodyDef.BodyType.DynamicBody
//        posicion = null
//        puedeRotar = false
//        data= null
//        bullet = false
//        gravedad = 1f
//        velocidad = vec2(0f, 0f)
//        fixtures.clear()
//        dispose.clear()
//    }


}