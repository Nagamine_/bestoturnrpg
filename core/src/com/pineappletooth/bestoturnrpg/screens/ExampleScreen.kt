package com.pineappletooth.bestoturnrpg.screens


import com.badlogic.gdx.Gdx.app
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.pineapple.physics.core.Mundo
import com.pineappletooth.bestoturnrpg.BestoTurnRPG
import com.pineappletooth.bestoturnrpg.componentes.*
import com.pineappletooth.bestoturnrpg.componentes.ia.BasicIAComponent
import com.pineappletooth.bestoturnrpg.constantes.ALTO
import com.pineappletooth.bestoturnrpg.constantes.ANCHO
import com.pineappletooth.bestoturnrpg.constantes.GUSANO
import com.pineappletooth.bestoturnrpg.constantes.TIERRA
import com.pineappletooth.bestoturnrpg.data.enemigo
import com.pineappletooth.bestoturnrpg.data.principal
import com.pineappletooth.bestoturnrpg.listeners.ConjuntoTeclas
import ktx.app.KtxScreen
import ktx.ashley.add
import ktx.ashley.entity
import ktx.graphics.use

class ExampleScreen : KtxScreen {
    // Notice no `lateinit var` - ExampleScreen has no create()
    // method and is constructed after LibGDX is fully initiated
    // in ExampleGame.create method.
    var contador = 0
    var tiempo = 0f
    var promedio = 60f
    val font = BitmapFont().apply {
        //data.scale(1f)
    }
    val juego = app.applicationListener as BestoTurnRPG
    val batch = SpriteBatch().apply {
        color = Color.WHITE

    }

    init {
     //   println(juego.viewport.screenHeight)


        //loadTerrain()// generarPiso(100,.5f)
        generarPiso()

        principal.crearPersonaje()
        enemigo.crearPersonaje()
    //    print(juego.camera.viewportWidth)



        //engine.assetManager.load()
    }

    override fun render(delta: Float) {
        juego.viewportInterfaz.apply()
        if(contador<60){
            contador++
            tiempo += delta
        }else {
            promedio = 1/(tiempo/60)
            contador = 0
            tiempo = 0f
        }
        batch.use(juego.cameraInterfaz.combined) {
            font.draw(it, promedio.toString(), 800f, 650f)
        }
        juego.viewport.apply()
    }


    override fun dispose() {
        // Will be automatically disposed of by the game instance.
        font.dispose()
        batch.dispose()
    }



    fun generarPiso(){
       for(i in 0 until juego.mundo.mapa.size){
           for(j in 0 until juego.mundo.mapa[i].size){
               juego.engine.add {
                   if(j==0) {
                       entity {
                           with<PosicionComponent>{
                               x=i*Mundo.TAMANIO_TILE.toFloat()
                               y=j*Mundo.TAMANIO_TILE.toFloat()
                           }
                           with<TamanioComponent>{
                               tamanioX=Mundo.TAMANIO_TILE.toFloat()
                               tamanioY=Mundo.TAMANIO_TILE.toFloat()
                           }
                           with<SpriteComponent> {
                               textura = TIERRA
                           }
                       }
                   }
               }
           }
       }
    }


}