package com.pineappletooth.bestoturnrpg.constantes

const val TESTASSET = "test.png"
const val ANIM = "hit06.png"
const val TEST = "test2.png"
const val MAGO = "mago"
const val GUSANO = "gusano"
const val MURO = "muro"
const val TIERRA = "tierra.png"
const val PRUEBA = "prueba.png"
const val BALA = "bala.png"
const val FONDO = "fondo.png"
const val FUEGO = "fireball.png"
const val VENENO = "veneno.png"