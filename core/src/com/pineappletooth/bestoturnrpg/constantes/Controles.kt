package com.pineappletooth.bestoturnrpg.constantes

import com.badlogic.gdx.Input

val MOVER_DERECHA = Input.Keys.RIGHT
val MOVER_IZQUIERDA = Input.Keys.LEFT
val SALTAR = Input.Keys.UP
val SKILL_1 = Input.Keys.Q
val SKILL_2 = Input.Keys.W
val SKILL_3 = Input.Keys.E
val SKILL_4 = Input.Keys.R
val MOVER_DERECHA2 = Input.Keys.D
val MOVER_IZQUIERDA2 = Input.Keys.A
val SALTAR2 = Input.Keys.W
val DISPARAR2 = Input.Keys.E
val MELEE2 = Input.Keys.R
val SKILL2_1 = Input.Keys.NUMPAD_1
val SKILL2_2 = Input.Keys.NUMPAD_2
val SKILL2_3 = Input.Keys.NUMPAD_3
val SKILL2_4 = Input.Keys.NUMPAD_4