package com.pineappletooth.bestoturnrpg.constantes

enum class Direccion {
    IZQUIERDA,
    DERECHA
}