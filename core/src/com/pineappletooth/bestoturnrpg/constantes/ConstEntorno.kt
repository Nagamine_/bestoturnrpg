package com.pineappletooth.bestoturnrpg.constantes

const val PIXEL_POR_METRO = 32f
const val ALTO = 720f//4 metros de alto//1080f / PIXEL_POR_METRO //1080f
const val ANCHO = 1280f//1920f / PIXEL_POR_METRO //1920f
const val GRAVEDAD = -30f
const val VEL_PLAYER = 450f

const val TAMANIO_TILE = 32
