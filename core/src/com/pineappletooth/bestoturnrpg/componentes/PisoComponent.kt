package com.pineappletooth.bestoturnrpg.componentes

import com.badlogic.ashley.core.Component
import com.badlogic.gdx.utils.Pool

class PisoComponent(var x : Float = 0.0f ,var y : Float = 0.0f) :Component , Pool.Poolable  {
    override fun reset() {
        x = 0.0f
        y = 0.0f
    }
}