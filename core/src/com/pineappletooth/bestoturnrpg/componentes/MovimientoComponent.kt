package com.pineappletooth.bestoturnrpg.componentes

import com.badlogic.ashley.core.Component
import com.badlogic.gdx.utils.Pool
import com.pineappletooth.bestoturnrpg.Accion
import com.pineappletooth.bestoturnrpg.constantes.Direccion
import com.pineappletooth.bestoturnrpg.listeners.InputInterfaz
import com.pineappletooth.bestoturnrpg.listeners.InputListener
import ktx.collections.gdxArrayOf
import ktx.collections.gdxListOf

class MovimientoComponent(
        var detenido:Boolean = true,
        var contactoPiso: Int = 0,
        var numSaltos : Int = 0,
        var puedeMover : Boolean = true
) : Component , Pool.Poolable {


    override fun reset() {
        detenido = true
        contactoPiso= 0
        numSaltos  = 0
        puedeMover = true
    }
}