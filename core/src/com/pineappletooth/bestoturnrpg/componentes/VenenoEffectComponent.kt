package com.pineappletooth.bestoturnrpg.componentes

import com.badlogic.ashley.core.Component
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.utils.Pool

class VenenoEffectComponent(var tiempo : Float = 0f , var damage : Int = 0) : Component,Pool.Poolable{
    var tiempoTranscurrido = 0f
    var tiempoCiclo = 0f
    var duracionCiclo = 1f
    var color: Color = Color.PURPLE
    override fun reset() {
        tiempo = 0f
        damage = 0
        tiempoCiclo=0f
        tiempoTranscurrido = 0f
        duracionCiclo = 1f
    }
}