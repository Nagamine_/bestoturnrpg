package com.pineappletooth.bestoturnrpg.componentes

import com.badlogic.ashley.core.Component
import com.badlogic.gdx.utils.Pool

class EnvenenadoComponent(var tiempo : Float = 0f, var damage : Int = 0) : Component , Pool.Poolable {
    override fun reset() {
        tiempo = 0f
        damage = 0
    }
}