package com.pineappletooth.bestoturnrpg.componentes.ia

import com.badlogic.ashley.core.Component
import com.badlogic.gdx.utils.Pool
import com.pineappletooth.bestoturnrpg.constantes.Direccion

class BasicIAComponent(var direccion: Direccion= Direccion.IZQUIERDA) :Component , Pool.Poolable{
    override fun reset() {
        direccion = Direccion.IZQUIERDA
    }
}