package com.pineappletooth.bestoturnrpg.componentes
import com.badlogic.ashley.core.Component
import com.badlogic.gdx.utils.Pool
import com.pineappletooth.bestoturnrpg.constantes.Direccion

class PosicionComponent(var x : Float = 0f, var y : Float = 0f, var z : Float = 0f ) : Component, Pool.Poolable  {
    var direccion = Direccion.DERECHA

    override fun reset() {
        x = 0.0f
        y = 0.0f
        z = 0.0f
        direccion = Direccion.DERECHA
    }
}