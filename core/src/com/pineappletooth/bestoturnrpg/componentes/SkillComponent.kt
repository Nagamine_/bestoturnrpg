package com.pineappletooth.bestoturnrpg.componentes

import com.badlogic.ashley.core.Component
import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.utils.Pool
import com.pineappletooth.bestoturnrpg.data.Skill

class SkillComponent (var skill1 : Skill? = null, var skill2 :  Skill? = null, var skill3 :  Skill? = null, var skill4 :  Skill? = null ) :Component , Pool.Poolable {
    override fun reset() {
        skill1 = null
        skill2 = null
        skill3 = null
        skill4 = null
    }
}