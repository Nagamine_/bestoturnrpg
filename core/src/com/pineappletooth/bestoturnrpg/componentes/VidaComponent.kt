package com.pineappletooth.bestoturnrpg.componentes

import com.badlogic.ashley.core.Component
import com.badlogic.gdx.utils.Pool

class VidaComponent(var vidaActual : Int = 100,  vidaTotal : Int = 100) : Component , Pool.Poolable  {
    var vidaTotal = vidaTotal
        set(value){
            field = value
            vidaActual = vidaTotal
        }
    override fun reset() {
        vidaActual = 100
        vidaTotal = 100
    }
}