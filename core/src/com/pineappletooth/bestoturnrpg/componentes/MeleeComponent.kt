package com.pineappletooth.bestoturnrpg.componentes

import com.badlogic.ashley.core.Component
import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.utils.Pool

class MeleeComponent : Component , Pool.Poolable {
    var origen : Entity? = null
    override fun reset() {
        origen = null
    }
}