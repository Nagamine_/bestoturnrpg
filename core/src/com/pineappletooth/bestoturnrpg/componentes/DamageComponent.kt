package com.pineappletooth.bestoturnrpg.componentes

import com.badlogic.ashley.core.Component
import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.utils.Pool

class DamageComponent (var damage : Int = 0, var origen : Entity? = null) : Component, Pool.Poolable  {
    override fun reset() {
        damage = 0
    }
}