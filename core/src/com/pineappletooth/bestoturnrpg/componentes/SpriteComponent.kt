package com.pineappletooth.bestoturnrpg.componentes

import com.badlogic.ashley.core.Component
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.utils.Pool

class SpriteComponent (var textura : String = "") : Component, Pool.Poolable  {
    override fun reset() {
        textura = ""
    }
}