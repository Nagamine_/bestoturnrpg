package com.pineappletooth.bestoturnrpg.componentes

import com.badlogic.ashley.core.Component
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.Animation
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.utils.Pool

class AnimacionComponent() : Component , Pool.Poolable {
    var animacion : Animation<TextureRegion>? = null
    var temporal : Boolean = false
    var start : Boolean = false
    var stateTime : Float = 0f
    override fun reset() {

        temporal= false
        animacion  = null
        start  = false
        stateTime  = 0f

    }
}