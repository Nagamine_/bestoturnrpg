package com.pineappletooth.bestoturnrpg.componentes

import com.badlogic.ashley.core.Component
import com.badlogic.gdx.utils.Pool

class ProyectilComponent(var velocidadX : Float = 0f, var velocidadY : Float = 0f) : Component, Pool.Poolable  {
    override fun reset() {
        velocidadX = 0f
        velocidadY = 0f
    }
}