package com.pineappletooth.bestoturnrpg.componentes

import com.badlogic.ashley.core.Component
import com.badlogic.gdx.utils.Pool

class BaseStatsComponent(var ataque : Int= 0, var defensa  : Int = 0 , var velocidad : Int = 0) : Component, Pool.Poolable {
    override fun reset() {
        ataque = 0
        defensa = 0
        velocidad = 0
    }

}