package com.pineappletooth.bestoturnrpg.componentes

import com.badlogic.ashley.core.Component
import com.badlogic.gdx.Gdx

import com.badlogic.gdx.utils.Pool
import com.pineapple.physics.core.Body

import com.pineappletooth.bestoturnrpg.BestoTurnRPG

class BodyComponent : Component , Pool.Poolable{
    var body : Body? = null
    override fun reset() {
        val g = Gdx.app.applicationListener as BestoTurnRPG
        body?.let { g.mundo.removeBody(it) }


        body = null
    }


    /*
    mover(entidad,pos){
        val x1 = entidad.x
        val y1 = entidad.y
        if(bloque.colides(entidad)){
            entidad.x =
        }
    }
     */
}