package com.pineappletooth.bestoturnrpg.componentes

import com.badlogic.ashley.core.Component
import com.badlogic.gdx.utils.Pool


class TamanioComponent(var tamanioX:Float = 32f, var tamanioY : Float = 32f) : Component, Pool.Poolable  {
    override fun reset() {
        tamanioX = 32f
        tamanioY = 32f
    }
}