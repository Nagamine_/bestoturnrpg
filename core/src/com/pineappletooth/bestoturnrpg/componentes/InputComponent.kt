package com.pineappletooth.bestoturnrpg.componentes

import com.badlogic.ashley.core.Component
import com.badlogic.gdx.utils.Pool
import com.pineappletooth.bestoturnrpg.constantes.*
import com.pineappletooth.bestoturnrpg.listeners.ConjuntoTeclas
import com.pineappletooth.bestoturnrpg.listeners.InputInterfaz
import com.pineappletooth.bestoturnrpg.listeners.InputListener


class InputComponent(numPlayer : Int = 1) : Component, Pool.Poolable{
    var conjuntoTeclas = ConjuntoTeclas()
    var numPlayer = numPlayer
        set(value){
            field = value
            conjuntoTeclas.AsignarTeclado(value)
        }
    var inputListener : InputInterfaz? = null
    override fun reset() {
        numPlayer = 1
    }
}