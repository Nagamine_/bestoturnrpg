package com.pineappletooth.bestoturnrpg

enum class Accion{
    IZQUIERDA,
    DERECHA,
    SALTO,
    DISPARAR,
    MELEE
}