package com.pineappletooth.bestoturnrpg.sistemas

import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.systems.IteratingSystem
import com.badlogic.gdx.Gdx
import com.pineappletooth.bestoturnrpg.BestoTurnRPG
import com.pineappletooth.bestoturnrpg.componentes.BodyComponent
import com.pineappletooth.bestoturnrpg.componentes.PosicionComponent
import com.pineappletooth.bestoturnrpg.managers.bodyMapper
import com.pineappletooth.bestoturnrpg.managers.posicionMapper
import ktx.ashley.allOf


/**
 * actualiza el componente posicion cada frame
 * la prioridad es despues de las fisicas, antes del dibujado
 */
class PosicionSystem(prioridad : Int= 0) : IteratingSystem(allOf(PosicionComponent::class,BodyComponent::class).get(), prioridad) {
    val juego = Gdx.app.applicationListener as BestoTurnRPG
    override fun processEntity(entity: Entity, deltaTime: Float) {
        bodyMapper[entity].body?.let {
            posicionMapper[entity].x = it.posicionx
            posicionMapper[entity].y = it.posiciony
        }

    }

}