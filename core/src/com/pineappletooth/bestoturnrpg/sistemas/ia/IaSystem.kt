package com.pineappletooth.bestoturnrpg.sistemas.ia

import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.systems.IteratingSystem
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Gdx.input
import com.badlogic.gdx.utils.IntMap
import com.pineappletooth.bestoturnrpg.Accion
import com.pineappletooth.bestoturnrpg.BestoTurnRPG
import com.pineappletooth.bestoturnrpg.componentes.BodyComponent
import com.pineappletooth.bestoturnrpg.componentes.MovimientoComponent
import com.pineappletooth.bestoturnrpg.componentes.PosicionComponent
import com.pineappletooth.bestoturnrpg.componentes.ia.BasicIAComponent
import com.pineappletooth.bestoturnrpg.constantes.*
import com.pineappletooth.bestoturnrpg.listeners.InputInterfaz
import com.pineappletooth.bestoturnrpg.listeners.InputListener
import com.pineappletooth.bestoturnrpg.listeners.Tecla
import com.pineappletooth.bestoturnrpg.managers.basicIaMapper
import com.pineappletooth.bestoturnrpg.managers.bodyMapper
import com.pineappletooth.bestoturnrpg.managers.posicionMapper
import ktx.ashley.allOf
import ktx.collections.gdxIntMap

class IaSystem(prioridad : Int = 0 ) : IteratingSystem(allOf(BodyComponent::class,BasicIAComponent::class).get(),prioridad) , InputInterfaz {
    override var inputMap = IntMap<Tecla>()
    val juego = Gdx.app.applicationListener as BestoTurnRPG
    var delta = 0f
    init{
        inputMap.put(MOVER_DERECHA,Tecla())
        inputMap.put(MOVER_IZQUIERDA,Tecla())
    }
    fun moverIzquierda(){
        inputMap[MOVER_DERECHA].presionado = false
        inputMap[MOVER_DERECHA].procesado = false
        inputMap[MOVER_IZQUIERDA].presionado = true
    }
    fun moverDerecha(){
        inputMap[MOVER_IZQUIERDA].presionado = false
        inputMap[MOVER_IZQUIERDA].procesado = false
        inputMap[MOVER_DERECHA].presionado = true
    }

    override fun processEntity(entity: Entity?, deltaTime: Float) {
        delta += deltaTime
        val b = bodyMapper[entity].body
        b?.let {
            if(basicIaMapper[entity].direccion == Direccion.IZQUIERDA){
                moverIzquierda()
                if(juego.mundo.getTile(b.posicionx,b.posiciony-1)?.solid == false){
                    moverDerecha()
                    delta = 0f
                    basicIaMapper[entity].direccion =Direccion.DERECHA
                }else if(it.velocidad.x == 0f){
                    moverDerecha()
                    delta = 0f
                    basicIaMapper[entity].direccion =Direccion.DERECHA
                } else if(delta > 2f){
                    delta = 0f
                    moverDerecha()
                    delta = 0f
                    basicIaMapper[entity].direccion =Direccion.DERECHA
                }
            }else{
                moverDerecha()
                if(juego.mundo.getTile(b.posicionx + b.tamanioX,b.posiciony-1)?.solid == false){
                    moverIzquierda()
                    delta = 0f
                    basicIaMapper[entity].direccion = Direccion.IZQUIERDA
                } else  if(it.velocidad.x == 0f){
                    moverIzquierda()
                    delta = 0f
                    basicIaMapper[entity].direccion = Direccion.IZQUIERDA
                }else if(delta > 2f){
                    delta = 0f
                    moverIzquierda()
                    basicIaMapper[entity].direccion = Direccion.IZQUIERDA
                }
            }

        }
    }

}