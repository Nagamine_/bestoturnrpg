package com.pineappletooth.bestoturnrpg.sistemas

import com.badlogic.ashley.core.Engine
import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.core.EntityListener
import com.badlogic.ashley.core.EntitySystem
import com.badlogic.gdx.Gdx
import com.pineappletooth.bestoturnrpg.BestoTurnRPG
import com.pineappletooth.bestoturnrpg.componentes.BodyComponent
import com.pineappletooth.bestoturnrpg.componentes.DamageComponent
import com.pineappletooth.bestoturnrpg.managers.*
import ktx.ashley.allOf

class DamageSystem(prioridad: Int = 0) : EntitySystem(prioridad), EntityListener {
    val juego = Gdx.app.applicationListener as BestoTurnRPG
    val vida = vidaMapper
    val dam = damageMapper
    val stat = baseStatMapper
    val familia = allOf(DamageComponent::class, BodyComponent::class).get()
    override fun addedToEngine(engine: Engine) {
        juego.engine.addEntityListener(familia, this)
        val f = engine.getEntitiesFor(familia)
        for (entity in f) {
            asignarColision(entity)
        }
    }

    override fun update(deltaTime: Float) {
        for(i in engine.getEntitiesFor(familia)){
          //  println(bodyMapper[i].body?.colisionElements)
        }

    }

    override fun entityRemoved(entity: Entity?) {
    }

    override fun entityAdded(entity: Entity) {
        asignarColision(entity)

    }

    fun asignarColision(entity: Entity) {

        bodyMapper[entity].body?.addColisionStartAction {
            val e = it.userData
            if (e != null) {
                if (e is Entity) {
                    if (vida.has(e)) {
                        if(dam.has(entity)) {
                            if (dam[entity].origen != null && stat.has(dam[entity].origen) && stat.has(e)) {
                                vida[e].vidaActual -= dam[entity].damage * stat[dam[entity].origen].ataque / (stat[e].defensa + 10)
                            } else {
                                vida[e].vidaActual -= dam[entity].damage
                                Gdx.app.log("[Daño]","Daño sin stats")
                            }
                        }
                    }
                }
            }
        }

    }
//           // val e = it.userData
//            if (e != null) {
//                if (e is Entity) {
//                    if (vida.has(e)) {
//                        if(dam[entity].origen != null && stat.has(dam[entity].origen) && stat.has(e)){
//                            vida[e].vidaActual -= dam[entity].damage*stat[dam[entity].origen].ataque / (stat[e].defensa+10)
//
//                        }else{
//                            vida[e].vidaActual -= dam[entity].damage
//                            println("DAÑO SIN STATS")
//                        }
//
//                    }
//                }
//            }


}