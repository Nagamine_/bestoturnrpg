package com.pineappletooth.bestoturnrpg.sistemas

import com.badlogic.ashley.core.EntitySystem
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.pineappletooth.bestoturnrpg.BestoTurnRPG
import com.pineappletooth.bestoturnrpg.constantes.ALTO
import com.pineappletooth.bestoturnrpg.constantes.ANCHO
import ktx.graphics.use

class FondoSystem(prioridad :Int = 0) : EntitySystem(prioridad) {
    val juego = Gdx.app.applicationListener as BestoTurnRPG
    val batch = SpriteBatch()
    override fun update(deltaTime: Float) {
        batch.use(juego.camera.combined){
           it.draw(juego.assets.fondo,-100f, -20f, ANCHO*100, ALTO*10, 1f, 1f, 1f, 1f)

        }
    }
}