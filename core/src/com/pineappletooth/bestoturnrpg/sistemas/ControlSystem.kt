package com.pineappletooth.bestoturnrpg.sistemas

import com.badlogic.ashley.core.Engine
import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.core.EntityListener
import com.badlogic.ashley.core.EntitySystem
import com.badlogic.ashley.utils.ImmutableArray
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input

import com.pineappletooth.bestoturnrpg.BestoTurnRPG
import com.pineappletooth.bestoturnrpg.componentes.*
import com.pineappletooth.bestoturnrpg.constantes.*
import com.pineappletooth.bestoturnrpg.data.ataque
import com.pineappletooth.bestoturnrpg.data.mortero
import com.pineappletooth.bestoturnrpg.data.proyectil
import com.pineappletooth.bestoturnrpg.data.veneno
import com.pineappletooth.bestoturnrpg.managers.*
import com.pineapple.physics.core.Body
import ktx.ashley.add
import ktx.ashley.allOf
import ktx.ashley.entity


class ControlSystem(prioridad: Int = 0) : EntitySystem(prioridad) {
    val juego = Gdx.app.applicationListener as BestoTurnRPG

    //val movimimiento = movimientoMapper
    lateinit var entidades: ImmutableArray<Entity>


    var vista = 0
    val asset = juego.assets.sprites
    val familia = allOf(MovimientoComponent::class, InputComponent::class).get()

    val listenerControl = object : EntityListener {
        var funcionBegin: (Body) -> Unit = {}
        var funcionEnd: (Body) -> Unit = {}
        override fun entityAdded(entity: Entity?) {

        }

        override fun entityRemoved(entity: Entity?) {

        }
    }

    override fun addedToEngine(engine: Engine) {
        super.addedToEngine(engine)
        engine.addEntityListener(familia, listenerControl)
        entidades = engine.getEntitiesFor(familia)



    }


    override fun update(deltaTime: Float) {
        //    println(1/deltaTime)
        super.update(deltaTime)

        for (entity in entidades) {
            inputMapper[entity].inputListener?.let { input ->
                val tecla = inputMapper[entity].conjuntoTeclas
                if(skillMapper.has(entity)) {

                    if (input.isJustPressed(tecla.keySkill_1)) {
                        skillMapper[entity].skill1?.crearSkill(entity)
                    }
                    if (input.isJustPressed(tecla.keySkill_2)) {
                        skillMapper[entity].skill2?.crearSkill(entity)
                    }
                    if (input.isJustPressed(tecla.keySkill_3)) {
                        skillMapper[entity].skill3?.crearSkill(entity)
                    }
                    if (input.isJustPressed(tecla.keySkill_4)) {
                        skillMapper[entity].skill4?.crearSkill(entity)
                    }
                }
                    if (input.isJustPressed(tecla.keyArriba)) {
                        saltar(bodyMapper[entity], movimientoMapper[entity])
                    }


                if (!input.isPressed(tecla.keyDerecha) && !input.isPressed(tecla.keyIzquierda)) {
                    movimientoMapper[entity].detenido = true
                }else if(input.isPressed(tecla.keyDerecha) && input.isPressed(tecla.keyIzquierda)){
                    if (input.isJustPressed(tecla.keyDerecha)) {
                        posicionMapper[entity].direccion = Direccion.DERECHA
                        movimientoMapper[entity].detenido = false
                    }
                    if (input.isJustPressed(tecla.keyIzquierda)) {
                        posicionMapper[entity].direccion = Direccion.IZQUIERDA
                        movimientoMapper[entity].detenido = false
                    }
                }else if(input.isPressed(tecla.keyIzquierda)){
                    input.isJustPressed(tecla.keyIzquierda)  //evitar mas llamadas  a is pressed
                    posicionMapper[entity].direccion = Direccion.IZQUIERDA
                    movimientoMapper[entity].detenido = false
                } else if(input.isPressed(tecla.keyDerecha)){
                    input.isJustPressed(tecla.keyDerecha) //evitar mas llamadas  a is pressed
                    posicionMapper[entity].direccion = Direccion.DERECHA
                    movimientoMapper[entity].detenido = false
                }



            }
            mover(entity)
            // fisica[entity].body?.linearVelocity =vector
        }

        if (Gdx.input.isKeyJustPressed(Input.Keys.SPACE)) {
            when (vista) {
                1 -> {
                    juego.engine.getSystem(DebugRendererSystem::class.java).setProcessing(false)
                }
                2 -> {
                    juego.engine.getSystem(SpriteSystem::class.java).setProcessing(false)
                    juego.engine.getSystem(DebugRendererSystem::class.java).setProcessing(true)
                }
                3 -> {
                    juego.engine.getSystem(SpriteSystem::class.java).setProcessing(true)
                    vista = 0
                }
            }
            vista++

        }


    }


    private fun saltar(fisc: BodyComponent, control: MovimientoComponent) {
        val b = fisc.body
        b?.let {
            if(it.isGrounded){
                control.numSaltos = 0
            }
            if(control.numSaltos<2){
                control.numSaltos++
                it.velocidad.y = 600f
            }

        }
    }

    fun mover(entity: Entity) {
        val b = bodyMapper[entity].body
        val velocidad = if(baseStatMapper.has(entity)) baseStatMapper[entity].velocidad * 45f else VEL_PLAYER
        if (b != null) {
            if(movimientoMapper[entity].detenido){
                b.velocidad.set(0f,b.velocidad.y)
            }else {
                if(movimientoMapper[entity].puedeMover) {
                    when (posicionMapper[entity].direccion) {
                        Direccion.DERECHA -> {
                            b.velocidad.set(velocidad,b.velocidad.y)
                        }
                        Direccion.IZQUIERDA -> {
                            b.velocidad.set(-velocidad,b.velocidad.y)
                        }
                    }
                }
            }

        }
    }




}

