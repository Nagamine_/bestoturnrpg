package com.pineappletooth.bestoturnrpg.sistemas

import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.systems.IteratingSystem
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.pineappletooth.bestoturnrpg.BestoTurnRPG
import com.pineappletooth.bestoturnrpg.componentes.VidaComponent
import com.pineappletooth.bestoturnrpg.data.enemigo
import com.pineappletooth.bestoturnrpg.managers.bodyMapper
import com.pineappletooth.bestoturnrpg.managers.tamanioMapper
import com.pineappletooth.bestoturnrpg.managers.vidaMapper
import com.pineappletooth.bestoturnrpg.screens.ExampleScreen
import ktx.ashley.allOf
import ktx.graphics.use
import ktx.math.vec2

class VidaSystem(prioridad : Int = 0) : IteratingSystem(allOf(VidaComponent::class).get(),prioridad) {
    val juego = Gdx.app.applicationListener as BestoTurnRPG
    val vida = vidaMapper
    val body = bodyMapper
    override fun processEntity(entity: Entity, deltaTime: Float) {
        if(vida[entity].vidaActual <= 0){
            juego.engine.removeEntity(entity)
            enemigo.crearPersonaje()
        }
    }
}