package com.pineappletooth.bestoturnrpg.sistemas

import com.badlogic.ashley.core.Engine
import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.core.EntityListener
import com.badlogic.ashley.core.EntitySystem
import com.badlogic.gdx.Gdx
import com.pineappletooth.bestoturnrpg.BestoTurnRPG
import com.pineappletooth.bestoturnrpg.componentes.BodyComponent
import com.pineappletooth.bestoturnrpg.componentes.ProyectilComponent
import com.pineappletooth.bestoturnrpg.constantes.Direccion
import com.pineappletooth.bestoturnrpg.managers.*
import ktx.ashley.allOf

class ProyectilSystem(var prioridad : Int = 0)  : EntitySystem(prioridad) , EntityListener{
    val juego = Gdx.app.applicationListener as BestoTurnRPG
    val familia = allOf(ProyectilComponent::class,BodyComponent::class).get()
    override fun addedToEngine(engine: Engine){
        juego.engine.addEntityListener(familia,this)
        val f = engine.getEntitiesFor(familia)
        for(entity in f){
            asignarColision(entity)
        }
    }
    override fun entityRemoved(entity: Entity?) {
    }

    override fun entityAdded(entity: Entity) {

        asignarColision(entity)
        if(posicionMapper[entity].direccion == Direccion.IZQUIERDA){
            bodyMapper[entity].body?.velocidad?.x = -proyectilMapper[entity].velocidadX
        }else{
            bodyMapper[entity].body?.velocidad?.x = proyectilMapper[entity].velocidadX
        }

        bodyMapper[entity].body?.velocidad?.y = proyectilMapper[entity].velocidadY

    }

    fun asignarColision(entity: Entity){

        bodyMapper[entity].body?.addColisionStartAction {

            juego.engine.removeEntity(entity)
        }
        bodyMapper[entity].body?.addColisionTile {
            juego.engine.removeEntity(entity)
        }

    }


}