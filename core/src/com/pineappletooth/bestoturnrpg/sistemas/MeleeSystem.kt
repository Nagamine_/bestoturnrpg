package com.pineappletooth.bestoturnrpg.sistemas

import com.badlogic.ashley.core.Engine
import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.core.EntityListener
import com.badlogic.ashley.core.EntitySystem
import com.badlogic.ashley.utils.ImmutableArray
import com.badlogic.gdx.Gdx
import com.pineappletooth.bestoturnrpg.BestoTurnRPG
import com.pineappletooth.bestoturnrpg.componentes.BodyComponent
import com.pineappletooth.bestoturnrpg.componentes.MeleeComponent
import com.pineappletooth.bestoturnrpg.constantes.Direccion
import com.pineappletooth.bestoturnrpg.managers.*
import ktx.ashley.allOf

class MeleeSystem(var prioridad : Int = 0)  : EntitySystem(prioridad) , EntityListener{
    val juego = Gdx.app.applicationListener as BestoTurnRPG
    val tamanioComponent = tamanioMapper
    val ataque = ataqueMapper
    val animacion = animaciomMapper
    val fisica = bodyMapper
    val posicion = posicionMapper
    val familia = allOf(MeleeComponent::class,BodyComponent::class).get()
    lateinit var f : ImmutableArray<Entity>
    override fun addedToEngine(engine: Engine){
        juego.engine.addEntityListener(familia,this)
        f = engine.getEntitiesFor(familia)
        for(entity in f){
            asignarColision(entity)
        }
    }
    override fun entityRemoved(entity: Entity?) {
    }

    override fun entityAdded(entity: Entity) {
        val pos = posicion[ataque[entity].origen]
        val tam = tamanioComponent[ataque[entity].origen]
        asignarColision(entity)

        bodyMapper[ataque[entity].origen].body?.let {
            it.velocidad.set(0f,0f)
        }

    }

    override fun update(deltaTime: Float) {
        for(entity in f){
            val anim = animacion[entity].animacion
            val pos = posicion[ataque[entity].origen]
            val tam = tamanioComponent[ataque[entity].origen]
           // val con = control[ataque[entity].origen]
           // fisica[entity].body?.position?.set(posicion[ataque[entity].origen].x + 101f,posicion[ataque[entity].origen].y + 101f)

            bodyMapper[entity].body?.let{
                if(bodyMapper.has(ataque[entity].origen)) {


                    bodyMapper[ataque[entity].origen].body?.let { it2 ->
                        it2.velocidad.set(0f,0f)
                        if(posicion[ataque[entity].origen].direccion == Direccion.DERECHA){
                            fisica[entity].body?.posicionx = pos.x + tam.tamanioX + 2f
                            fisica[entity].body?.posiciony =pos.y
                        }else if(posicion[ataque[entity].origen].direccion == Direccion.IZQUIERDA){
                            fisica[entity].body?.posicionx = pos.x - tam.tamanioX - 2f
                            fisica[entity].body?.posiciony =pos.y
                        }else{

                        }
                    }
                }
            }

            if(anim != null){
                if(movimientoMapper.has(ataque[entity].origen)) {
                    if (anim.isAnimationFinished(animacion[entity].stateTime)) {
                        movimientoMapper[ataque[entity].origen].puedeMover = true
                        bodyMapper[entity].body?.let {
                            //it.isActive = false
                        }
                        engine.removeEntity(entity)

                    } else {
                            movimientoMapper[ataque[entity].origen].puedeMover = false
                            bodyMapper[ataque[entity].origen].body?.let {
                                it.velocidad.set(0f,0f)
                            }
                    }
                }
            }
        }
    }
    fun asignarColision(entity: Entity){

    }


}