package com.pineappletooth.bestoturnrpg.sistemas

import com.badlogic.ashley.core.Engine
import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.core.EntityListener
import com.badlogic.ashley.systems.IteratingSystem
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.g2d.Animation
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.pineappletooth.bestoturnrpg.BestoTurnRPG
import com.pineappletooth.bestoturnrpg.componentes.AnimacionComponent
import com.pineappletooth.bestoturnrpg.componentes.PosicionComponent
import com.pineappletooth.bestoturnrpg.constantes.Direccion
import com.pineappletooth.bestoturnrpg.managers.animaciomMapper
import com.pineappletooth.bestoturnrpg.managers.posicionMapper
import com.pineappletooth.bestoturnrpg.managers.tamanioMapper
import ktx.ashley.allOf
import ktx.collections.gdxArrayOf
import ktx.graphics.use


class AnimacionSystem(prioridad : Int = 0) : IteratingSystem(allOf(AnimacionComponent::class).get(),prioridad) {
    val juego = Gdx.app.applicationListener as BestoTurnRPG
    val animacion = animaciomMapper
    val tamanioComponent = tamanioMapper
    val posicionComponent = posicionMapper
    val batch = SpriteBatch()

    override fun processEntity(entity: Entity?, deltaTime: Float) {
        val anim = animacion[entity].animacion
        if(anim!= null){
            if(animacion[entity].start) {
                animacion[entity].stateTime += Gdx.graphics.deltaTime // Accumulate elapsed animation time
                // Get current frame of animation for the current stateTime

                // Get current frame of animation for the current stateTime
                val currentFrame: TextureRegion = anim.getKeyFrame(animacion[entity].stateTime, !animacion[entity].temporal)

                batch.use(juego.camera.combined) {
                    if(posicionComponent[entity].direccion == Direccion.IZQUIERDA) {
                        it.draw(currentFrame, posicionComponent[entity].x , posicionComponent[entity].y , tamanioComponent[entity].tamanioX, tamanioComponent[entity].tamanioY)
                    }else{
                        it.draw(currentFrame, posicionComponent[entity].x + tamanioComponent[entity].tamanioX, posicionComponent[entity].y ,- tamanioComponent[entity].tamanioX,tamanioComponent[entity].tamanioY)
                    }
                }
            } else {
                animacion[entity].stateTime = 0f
            }
            if(animacion[entity].temporal){
                if(anim.isAnimationFinished(animacion[entity].stateTime)){
                    animacion[entity].start = false
                }
            }
        }
    }

}