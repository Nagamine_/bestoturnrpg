package com.pineappletooth.bestoturnrpg.sistemas

import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.systems.IteratingSystem
import com.badlogic.gdx.Gdx.app
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.pineappletooth.bestoturnrpg.BestoTurnRPG
import com.pineappletooth.bestoturnrpg.componentes.PosicionComponent
import com.pineappletooth.bestoturnrpg.componentes.SpriteComponent
import com.pineappletooth.bestoturnrpg.componentes.TamanioComponent
import com.pineappletooth.bestoturnrpg.constantes.Direccion
import com.pineappletooth.bestoturnrpg.managers.bodyMapper
import com.pineappletooth.bestoturnrpg.managers.posicionMapper
import com.pineappletooth.bestoturnrpg.managers.spriteMapper
import com.pineappletooth.bestoturnrpg.managers.tamanioMapper
import ktx.ashley.allOf
import ktx.graphics.use

/**
 * dibuja los sprite
 * la prioridad es al ultimo, despues del calculo de fisicas y de la actualizacion de posicion
 */
class SpriteSystem( prioridad : Int = 0)  : IteratingSystem(allOf(SpriteComponent::class,PosicionComponent::class,TamanioComponent::class).get(),prioridad){
    val juego = app.applicationListener as BestoTurnRPG
    val batch = SpriteBatch()
    val posicion = posicionMapper
    val sprite = spriteMapper
    val tamanio = tamanioMapper
    val body = bodyMapper
    override fun processEntity(entity: Entity?, deltaTime: Float) {
            val textura = juego.assets.sprites[sprite[entity].textura]

            batch.use(juego.camera.combined) {
                if(posicion[entity].direccion == Direccion.DERECHA) {
                    it.draw(textura, posicion[entity].x , posicion[entity].y, tamanio[entity].tamanioX, tamanio[entity].tamanioY)
                } else{
                    it.draw(textura, posicion[entity].x + tamanio[entity].tamanioX, posicion[entity].y , -tamanio[entity].tamanioX, tamanio[entity].tamanioY)
                }

        }
    }

}