package com.pineappletooth.bestoturnrpg.sistemas

import com.badlogic.ashley.core.EntitySystem
import com.badlogic.gdx.Gdx

import com.pineappletooth.bestoturnrpg.BestoTurnRPG

class DebugRendererSystem(prioridad : Int = 0)  :EntitySystem(prioridad){

    val juego = Gdx.app.applicationListener as BestoTurnRPG
    override fun update(deltaTime: Float) {
        juego.mundo.dibujar(juego.camera.combined)
    }
}