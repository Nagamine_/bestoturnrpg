package com.pineappletooth.bestoturnrpg.sistemas

import com.badlogic.ashley.core.Engine
import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.core.EntityListener
import com.badlogic.ashley.systems.IteratingSystem
import com.badlogic.gdx.Gdx
import com.pineappletooth.bestoturnrpg.BestoTurnRPG
import com.pineappletooth.bestoturnrpg.componentes.BodyComponent
import com.pineappletooth.bestoturnrpg.componentes.EnvenenadoComponent
import com.pineappletooth.bestoturnrpg.componentes.VenenoEffectComponent
import com.pineappletooth.bestoturnrpg.managers.bodyMapper
import com.pineappletooth.bestoturnrpg.managers.venenoEffectMapper
import com.pineappletooth.bestoturnrpg.managers.vidaMapper

import ktx.ashley.allOf

class VenenoSystem : IteratingSystem(allOf(BodyComponent::class, EnvenenadoComponent::class).get()),EntityListener{
    val juego = Gdx.app.applicationListener as BestoTurnRPG
    override fun processEntity(entity: Entity?, deltaTime: Float) {

    }

    override fun addedToEngine(engine: Engine?) {
        super.addedToEngine(engine)
        juego.engine.addEntityListener(family,this)
    }

    override fun entityRemoved(entity: Entity?) {

    }

    override fun entityAdded(entity: Entity) {
        bodyMapper[entity].body?.addColisionStartAction {
            val e = it.userData as Entity?

            if(e != null){
                if(!venenoEffectMapper.has(e)&& vidaMapper.has(e)){
                    e.add(VenenoEffectComponent(10f,10))
               }
            }
        }
  //      colisionMapper[entity].beginContactList.add {
//            val e = it.userData as Entity?
//
//            if(e != null){
//                if(vidaMapper.has(e)){
//                    e.add(VenenoEffectComponent(10f,10))
//                }
//            }

    //    }
    }

}