package com.pineappletooth.bestoturnrpg.sistemas

import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.systems.IteratingSystem
import com.badlogic.gdx.Gdx
import com.pineappletooth.bestoturnrpg.BestoTurnRPG
import com.pineappletooth.bestoturnrpg.componentes.VenenoEffectComponent
import com.pineappletooth.bestoturnrpg.componentes.VidaComponent
import com.pineappletooth.bestoturnrpg.managers.venenoEffectMapper
import com.pineappletooth.bestoturnrpg.managers.vidaMapper
import ktx.ashley.allOf

class VenenoEffectSystem (prioridad : Int = 0) : IteratingSystem(allOf(VenenoEffectComponent::class,VidaComponent::class).get(),prioridad){
    val juego = Gdx.app.applicationListener as BestoTurnRPG
    override fun processEntity(entity: Entity, deltaTime: Float) {
        venenoEffectMapper[entity].tiempoTranscurrido += deltaTime
        venenoEffectMapper[entity].tiempoCiclo += deltaTime
        while(venenoEffectMapper[entity].tiempoCiclo>= venenoEffectMapper[entity].duracionCiclo ){
            venenoEffectMapper[entity].tiempoCiclo -=venenoEffectMapper[entity].duracionCiclo
            vidaMapper[entity].vidaActual -= venenoEffectMapper[entity].damage
        }
        if(venenoEffectMapper[entity].tiempoTranscurrido >= venenoEffectMapper[entity].tiempo){
            entity.remove(VenenoEffectComponent::class.java)
        }
    }

}