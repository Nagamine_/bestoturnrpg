package com.pineappletooth.bestoturnrpg.sistemas

import com.badlogic.ashley.core.Engine
import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.core.EntitySystem
import com.badlogic.ashley.systems.IteratingSystem
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.pineappletooth.bestoturnrpg.BestoTurnRPG
import com.pineappletooth.bestoturnrpg.componentes.*
import com.pineappletooth.bestoturnrpg.managers.bodyMapper
import com.pineappletooth.bestoturnrpg.managers.tamanioMapper
import com.pineappletooth.bestoturnrpg.managers.venenoEffectMapper
import com.pineappletooth.bestoturnrpg.managers.vidaMapper
import ktx.ashley.allOf
import ktx.ashley.oneOf
import ktx.graphics.use
import ktx.math.vec2

class DibujarEfectoSystem(prioridad: Int = 0) : IteratingSystem(allOf(BodyComponent::class).oneOf(VenenoEffectComponent::class, VidaComponent::class).get(), prioridad) {
    private val juego = Gdx.app.applicationListener as BestoTurnRPG
    private val shapeRenderer = ShapeRenderer()

    override fun processEntity(entity: Entity?, deltaTime: Float) {

        shapeRenderer.projectionMatrix = juego.camera.combined
        if (bodyMapper.has(entity)) {
            val b = bodyMapper[entity].body
            if (b != null) {
                val posicion = vec2(b.posicionx, b.posiciony)
                var tam = posicion.y + b.tamanioY + b.tamanioY / 10
                if (vidaMapper.has(entity)) {
                    if (vidaMapper[entity].vidaActual < vidaMapper[entity].vidaTotal) {
                        shapeRenderer.use(ShapeRenderer.ShapeType.Filled) {
                            shapeRenderer.color = Color.LIGHT_GRAY
                            shapeRenderer.rect(posicion.x, tam, b.tamanioX, b.tamanioY / 10)
                            shapeRenderer.color = Color.RED
                            shapeRenderer.rect(posicion.x, tam, b.tamanioX * vidaMapper[entity].vidaActual / vidaMapper[entity].vidaTotal, b.tamanioY / 10)
                        }
                        tam += b.tamanioY / 10
                    }
                }
                if (venenoEffectMapper.has(entity)) {
                    shapeRenderer.use(ShapeRenderer.ShapeType.Filled) {
                        it.color = venenoEffectMapper[entity].color
                        it.rect(posicion.x,tam,b.tamanioX/3,b.tamanioY/10)
                    }
                }
            }
        }

    }
}