package com.pineappletooth.bestoturnrpg.sistemas

import com.badlogic.ashley.core.Engine
import com.badlogic.ashley.core.Entity
import com.badlogic.ashley.core.EntitySystem
import com.badlogic.ashley.utils.ImmutableArray
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.math.Vector3
import com.pineappletooth.bestoturnrpg.BestoTurnRPG
import com.pineappletooth.bestoturnrpg.componentes.CameraFocusComponent
import com.pineappletooth.bestoturnrpg.componentes.MovimientoComponent
import com.pineappletooth.bestoturnrpg.componentes.PosicionComponent
import com.pineappletooth.bestoturnrpg.managers.posicionMapper
import ktx.ashley.allOf


class CameraSystem(prioridad : Int= 0) : EntitySystem(prioridad){
    lateinit var principal : ImmutableArray<Entity>
    val juego = Gdx.app.applicationListener as BestoTurnRPG
    val posicionComponent = posicionMapper
    override fun addedToEngine(engine: Engine) {
        principal = engine.getEntitiesFor(allOf(CameraFocusComponent::class).get())

    }
    override fun update(deltaTime: Float) {
        for(c in principal) {

            val pos = posicionComponent[c]
              //  juego.camera.position.set(pos.x,pos.y,0f)
            val lerp = 6f
            val position: Vector3 = juego.camera.position
            position.x += (pos.x - position.x) * lerp * deltaTime
            position.y += (pos.y - position.y) * lerp * deltaTime
            juego.camera.update()

        }
    }
}