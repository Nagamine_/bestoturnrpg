package com.pineappletooth.bestoturnrpg.listeners

import com.pineappletooth.bestoturnrpg.constantes.*

class ConjuntoTeclas {
    var keyDerecha = MOVER_DERECHA
    var keyIzquierda = MOVER_IZQUIERDA
    var keyArriba = SALTAR
    var keySkill_1 = SKILL_1
    var keySkill_2 = SKILL_2
    var keySkill_3 = SKILL_3
    var keySkill_4 = SKILL_4

    fun AsignarTeclado(numplayer: Int) {
        when (numplayer) {
            1 -> {
                keyDerecha = MOVER_DERECHA
                keyIzquierda = MOVER_IZQUIERDA
                keyArriba = SALTAR
                keySkill_1 = SKILL_1
                keySkill_2 = SKILL_2
                keySkill_3= SKILL_3
                keySkill_4 = SKILL_4
            }
            2 -> {
                keyDerecha = MOVER_DERECHA2
                keyIzquierda = MOVER_IZQUIERDA2
                keyArriba = SALTAR2
                keySkill_1 = DISPARAR2
                keySkill_2 = MELEE2
                keySkill_1 = SKILL2_1
                keySkill_2 = SKILL2_2
                keySkill_3= SKILL2_3
                keySkill_4 = SKILL2_4
            }
        }
    }

}