package com.pineappletooth.bestoturnrpg.listeners

import com.badlogic.gdx.utils.IntMap
import ktx.collections.GdxMap
import ktx.collections.gdxIntMap

interface InputInterfaz {
    var inputMap : IntMap<Tecla>
    fun isJustPressed(keyCode : Int) : Boolean  {
        if(inputMap.containsKey(keyCode)) {
            if (inputMap[keyCode].presionado) {
                if (!inputMap[keyCode].procesado) {
                    inputMap[keyCode].procesado = true
                    return true
                }
            }
        }
        return false
    }
    fun isPressed(keyCode: Int) : Boolean {
        if(inputMap.containsKey(keyCode)) {
            return inputMap[keyCode].presionado
        } else{
            return false
        }
    }
}