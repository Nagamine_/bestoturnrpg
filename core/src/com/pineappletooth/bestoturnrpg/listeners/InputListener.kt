package com.pineappletooth.bestoturnrpg.listeners

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.InputAdapter
import com.badlogic.gdx.utils.IntMap
import com.pineappletooth.bestoturnrpg.constantes.*
import ktx.collections.*
import kotlin.collections.contains

class InputListener : InputAdapter() ,InputInterfaz {
    override var inputMap = IntMap<Tecla>()

    init{
        Gdx.input.inputProcessor = this
//        inputMap.put(MOVER_DERECHA,Tecla())
//        inputMap.put(MOVER_IZQUIERDA,Tecla())
//        inputMap.put(SALTAR,Tecla())
//        inputMap.put(DISPARAR,Tecla())
//        inputMap.put(MELEE,Tecla())
//        inputMap.put(MOVER_DERECHA2,Tecla())
//        inputMap.put(MOVER_IZQUIERDA2,Tecla())
//        inputMap.put(SALTAR2,Tecla())
//        inputMap.put(DISPARAR2,Tecla())
//        inputMap.put(MELEE2,Tecla())
    }

    override fun keyDown(keycode: Int): Boolean {
        if(!inputMap.containsKey(keycode)) {
            inputMap.put(keycode,Tecla())
            //println("$keycode  ${inputMap[keycode].presionado}  ${inputMap[keycode].procesado}")
        }else{
        }
       // inputMap.put(keycode,Tecla())
        inputMap[keycode].presionado = true
        return true
    }
    override fun keyUp(keycode: Int): Boolean {
        if(inputMap.containsKey(keycode)) {
            inputMap[keycode].presionado = false
            inputMap[keycode].procesado = false
        }
        return true
    }
}