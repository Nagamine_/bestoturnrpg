package com.pineappletooth.bestoturnrpg.managers

import com.pineappletooth.bestoturnrpg.componentes.*
import com.pineappletooth.bestoturnrpg.componentes.ia.BasicIAComponent
import ktx.ashley.mapperFor

val posicionMapper = mapperFor<PosicionComponent>()
val tamanioMapper = mapperFor<TamanioComponent>()
val entidadMapper = mapperFor<EntidadComponent>()
val spriteMapper = mapperFor<SpriteComponent>()
val movimientoMapper = mapperFor<MovimientoComponent>()
val bodyMapper = mapperFor<BodyComponent>()
val pisoMapper = mapperFor<PisoComponent>()
val vidaMapper = mapperFor<VidaComponent>()
val proyectilMapper = mapperFor<ProyectilComponent>()
val animaciomMapper = mapperFor<AnimacionComponent>()
val ataqueMapper = mapperFor<MeleeComponent>()
val damageMapper = mapperFor<DamageComponent>()
val baseStatMapper = mapperFor<BaseStatsComponent>()
val inputMapper = mapperFor<InputComponent>()
val venenoMapper = mapperFor<EnvenenadoComponent>()
val venenoEffectMapper = mapperFor<VenenoEffectComponent>()
val skillMapper = mapperFor<SkillComponent>()
val basicIaMapper = mapperFor<BasicIAComponent>()
