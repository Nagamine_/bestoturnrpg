package com.pineappletooth.bestoturnrpg.managers

import com.badlogic.gdx.assets.AssetManager
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.Animation
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.pineappletooth.bestoturnrpg.constantes.*
import ktx.assets.getAsset
import ktx.assets.load
import ktx.collections.gdxArrayOf
import ktx.collections.gdxMapOf
import ktx.collections.set


class Assets {
    val manager = AssetManager()
    val sprites = gdxMapOf<String,TextureRegion>()
    val animaciones = gdxMapOf<String,Animation<TextureRegion>>()
    val anim : Texture
    val fondo : Texture

    init {
        manager.load<Texture>(TESTASSET)
        manager.load<Texture>(ANIM)
        manager.load<Texture>(TEST)
        manager.load<Texture>(TIERRA)
        manager.load<Texture>(PRUEBA)
        manager.load<Texture>(BALA)
        manager.load<Texture>(FONDO)
        manager.load<Texture>(FUEGO)
        manager.load<Texture>(VENENO)
        manager.finishLoading()
        anim = manager.getAsset(ANIM)
        val textura : Texture = manager.getAsset(TESTASSET)
        val p : Texture = manager.getAsset(PRUEBA)
        val g : Texture = manager.getAsset(TIERRA)
        val b : Texture = manager.getAsset(BALA)
        val f : Texture = manager.getAsset(FUEGO)
        val v : Texture = manager.getAsset(VENENO)
        fondo = manager.getAsset(FONDO)
        fondo.setWrap(Texture.TextureWrap.Repeat,Texture.TextureWrap.Repeat)

        sprites[GUSANO] = TextureRegion(textura,32 * 6, 32 * 19, 32, 32)
        sprites[MAGO] = TextureRegion(textura, 32 * 7, 32 * 18, 32, 32)
        sprites[MURO] = TextureRegion(textura, 32 * 2, 32 * 2, 32, 32)
        sprites[TIERRA] = TextureRegion(g)
        sprites[MAGO] = TextureRegion(p, 16 * 4, 16 * 8, 16, 16)
        sprites[GUSANO] = TextureRegion(p, 16 * 2, 16 * 10, 16, 16)
        sprites[BALA] = TextureRegion(b)
        sprites[FUEGO] = TextureRegion(f)
        sprites[VENENO] = TextureRegion(v)

        animaciones[ANIM] = Animacion(anim,5,2,0.025f).crearAnimacion()

    }



}