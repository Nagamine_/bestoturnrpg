package com.pineappletooth.bestoturnrpg.managers

import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.Animation
import com.badlogic.gdx.graphics.g2d.TextureRegion
import ktx.collections.gdxArrayOf

class Animacion(var texture: Texture, var filas : Int, var columnas : Int , var frameDuracion : Float) {
    fun crearAnimacion(): Animation<TextureRegion> {
        val tmp = TextureRegion.split(texture,
                texture.width / filas,
                texture.height / columnas)

        // Place the regions into a 1D array in the correct order, starting from the top
        // left, going across first. The Animation constructor requires a 1D array.

        // Place the regions into a 1D array in the correct order, starting from the top
        // left, going across first. The Animation constructor requires a 1D array.
        val walkFrames = gdxArrayOf<TextureRegion>()
        for (i in 0 until columnas) {
            for (j in 0 until filas) {
                walkFrames.add (tmp[i][j])
            }
        }

        return Animation<TextureRegion>(frameDuracion, walkFrames)
    }
}