package com.pineappletooth.bestoturnrpg.data

import com.badlogic.gdx.Gdx
import com.pineappletooth.bestoturnrpg.BestoTurnRPG
import com.pineappletooth.bestoturnrpg.componentes.*
import com.pineappletooth.bestoturnrpg.componentes.ia.BasicIAComponent
import com.pineappletooth.bestoturnrpg.constantes.ALTO
import com.pineappletooth.bestoturnrpg.constantes.ANCHO
import com.pineappletooth.bestoturnrpg.constantes.GUSANO
import com.pineappletooth.bestoturnrpg.constantes.MAGO
import com.pineappletooth.bestoturnrpg.listeners.ConjuntoTeclas
import com.pineappletooth.bestoturnrpg.managers.tamanioMapper

import com.pineappletooth.bestoturnrpg.data.ataque as ataqueSkill
import ktx.ashley.add
import ktx.ashley.entity
import ktx.math.vec2

class Personaje {
    val juego = Gdx.app.applicationListener as BestoTurnRPG
    var sprite = ""
    var posicion = vec2(10f,10f)
    var tamanio = vec2(64f,64f)
    var damage = 0f
    var vida = 100
    var ataque = 100
    var defensa = 100
    var velocidad = 100
    var player = 0 // 0=ia
    fun setStats(vida : Int , ataque : Int , defensa : Int , velocidad : Int){
        this.vida = vida
        this.ataque = ataque
        this.defensa = defensa
        this.velocidad = velocidad
    }
    fun crearPersonaje() {
        juego.engine.add {
            entity {
                with<MovimientoComponent>()
                with<SpriteComponent> {
                    textura = sprite
                }
                with<VidaComponent> {
                    vidaActual = vida
                    vidaTotal = vida
                }
                with<TamanioComponent> {
                    tamanioX = tamanio.x
                    tamanioY = tamanio.y
                }
                with<PosicionComponent> {
                    x = posicion.x
                    y = posicion.y
                }
                with<BaseStatsComponent> {
                    ataque = this@Personaje.ataque
                    defensa = this@Personaje.defensa
                    velocidad = this@Personaje.velocidad
                }

                with<BodyComponent> {
                    body =juego.mundo.addBody(posicion.x,posicion.y, tamanioMapper[this@entity.entity].tamanioX, tamanioMapper[this@entity.entity].tamanioY)
                    body?.userData = this@entity.entity
                }
                with<SkillComponent>{
                    skill1 = ataqueSkill
                    skill2 = proyectil
                    skill3 = mortero
                    skill4 = veneno
                }
                if(player>0){
                    with<CameraFocusComponent>()
                    with<InputComponent> {
                        inputListener = juego.inputListener
                        numPlayer = player
                        conjuntoTeclas = ConjuntoTeclas().apply {  AsignarTeclado(player)}
                    }
                }else{
                    with<InputComponent> {
                        this.inputListener = juego.ia
                        this.conjuntoTeclas = ConjuntoTeclas().also { it.AsignarTeclado(1) }
                    }
                    with<BasicIAComponent>()

                }
                if(damage>0){
                    with<DamageComponent>{
                        damage = 120
                        origen = this@entity.entity
                    }
                }
            }
        }
    }

}