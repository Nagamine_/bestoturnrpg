package com.pineappletooth.bestoturnrpg.data

import com.badlogic.ashley.core.PooledEngine
import com.pineappletooth.bestoturnrpg.constantes.FUEGO
import com.pineappletooth.bestoturnrpg.constantes.VENENO
import ktx.ashley.entity

val ataque= Skill(Skill.Tipo.MELEE,20)
val proyectil = Skill(Skill.Tipo.PROYECTIL,20)
val veneno = Skill(Skill.Tipo.PROYECTIL,0).apply {
    efectos.add(Skill.Efecto(Skill.TipoEfecto.VENENO,15f,5))
    asset = VENENO
}
val mortero = Skill(Skill.Tipo.PROYECTIL,50).apply {
    velocidad.set(500f,500f)
    gravedad = true
    tamanio.set(15f,15f)
    asset = FUEGO
}