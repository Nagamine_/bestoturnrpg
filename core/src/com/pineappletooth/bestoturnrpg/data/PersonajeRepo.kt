package com.pineappletooth.bestoturnrpg.data

import com.pineappletooth.bestoturnrpg.constantes.GUSANO
import com.pineappletooth.bestoturnrpg.constantes.MAGO

val principal = Personaje().apply {
    setStats(100,12,8,10)
    sprite = MAGO
    posicion.set(45f,45f)
    player = 1
}
val enemigo = Personaje().apply {
    setStats(100,8,9,7)
    sprite = GUSANO
    posicion.set(450f,200f)
    player = 0
    damage = 60f
}