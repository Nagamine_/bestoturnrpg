package com.pineappletooth.bestoturnrpg.data

import com.badlogic.ashley.core.Entity
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.math.Vector2
import com.pineappletooth.bestoturnrpg.BestoTurnRPG
import com.pineappletooth.bestoturnrpg.componentes.*
import com.pineappletooth.bestoturnrpg.constantes.ANIM
import com.pineappletooth.bestoturnrpg.constantes.BALA
import com.pineappletooth.bestoturnrpg.managers.bodyMapper
import com.pineappletooth.bestoturnrpg.managers.posicionMapper
import com.pineappletooth.bestoturnrpg.managers.tamanioMapper
import ktx.ashley.add
import ktx.ashley.entity
import ktx.collections.gdxArrayOf
import ktx.math.vec2


class Skill(var tipo: Tipo, var damage: Int) {
    private val juego = Gdx.app.applicationListener as BestoTurnRPG
    var asset = BALA
    val efectos = gdxArrayOf<Efecto>()
    var velocidad = Vector2(600f,0f)
    var gravedad = false
    var tamanio = vec2(30f,10f)
    class Efecto(var tipo : TipoEfecto,var tiempo : Float,var damage: Int)
    enum class Tipo {
        MELEE,
        PROYECTIL
    }

    enum class TipoEfecto {
        VENENO
    }


    fun crearSkill(entity: Entity): Entity {
        lateinit var entidad : Entity
        juego.engine.add {
            entidad = entity {
                if (tipo == Tipo.MELEE) {
                    with<AnimacionComponent> {
                        this.animacion = juego.assets.animaciones[ANIM]
                        this.temporal = true
                        this.start = true
                    }
                    with<PosicionComponent> {
                        //manejado en melee system
                        direccion = if (posicionMapper[entity].direccion == com.pineappletooth.bestoturnrpg.constantes.Direccion.DERECHA) {

                            com.pineappletooth.bestoturnrpg.constantes.Direccion.DERECHA
                        } else {

                            com.pineappletooth.bestoturnrpg.constantes.Direccion.IZQUIERDA
                        }
                    }
                    with<TamanioComponent> {
                        tamanioX = 64f
                        tamanioY = 64f
                    }
                    with<BodyComponent> {
                        body = juego.mundo.addBody(60f,90f,64f,64f)
                        body?.userData = this@entity.entity
                        body?.solid = false
                        body?.gravedad = false
                    }
                    with<MeleeComponent> {
                        origen = entity
                    }
                } else if (tipo == Tipo.PROYECTIL) {
                    with<SpriteComponent> {
                        textura = asset
                    }
                    with<TamanioComponent> {
                        tamanioX = tamanio.x
                        tamanioY = tamanio.y
                    }
                    with<PosicionComponent> {
                        if (posicionMapper[entity].direccion == com.pineappletooth.bestoturnrpg.constantes.Direccion.DERECHA) {
                            x = posicionMapper[entity].x+ tamanioMapper[entity].tamanioX
                            y = posicionMapper[entity].y+ tamanioMapper[entity].tamanioY/2
                            direccion = com.pineappletooth.bestoturnrpg.constantes.Direccion.DERECHA
                        } else {
                            x = posicionMapper[entity].x - tamanio.x
                            y = posicionMapper[entity].y + tamanioMapper[entity].tamanioY/2
                            direccion = com.pineappletooth.bestoturnrpg.constantes.Direccion.IZQUIERDA
                        }
                    }

                    with<ProyectilComponent> {
                        velocidadX = velocidad.x
                        velocidadY = velocidad.y
                    }
                    with<BodyComponent> {
                        body = juego.mundo.addBody(
                                posicionMapper[this@entity.entity].x,
                                posicionMapper[this@entity.entity].y,
                                tamanioMapper[this@entity.entity].tamanioX,
                                tamanioMapper[this@entity.entity].tamanioY
                        )
                       // println("${posicionMapper[this@entity.entity].x} ${posicionMapper[this@entity.entity].y}")
                        body?.let {
                            it.userData = this@entity.entity
                            it.gravedad = gravedad

                        }
                    }
                }
            }
        }
        entidad.add(DamageComponent(damage,entity))
        for(e in efectos){
            if(e.tipo==TipoEfecto.VENENO){
                entidad.add(EnvenenadoComponent(e.tiempo,e.damage))
            }
        }
        return entidad
    }
}
