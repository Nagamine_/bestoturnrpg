package com.pineapple.physics

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input
import com.badlogic.gdx.graphics.Camera
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.utils.viewport.ExtendViewport
import com.badlogic.gdx.utils.viewport.Viewport
import ktx.app.KtxApplicationAdapter
import ktx.app.clearScreen
import ktx.math.vec3
import com.pineapple.physics.core.Body
import com.pineapple.physics.core.Mundo
import com.pineapple.physics.utils.crearMundo

class Main : KtxApplicationAdapter {
    val alto = 720
    val ancho = 1280

    lateinit var shapeRenderer : ShapeRenderer

    lateinit var camera : Camera
    lateinit var viewport: Viewport
    lateinit var mundo : Mundo
     var salto = 0
    lateinit var per2 : Body
    lateinit var personaje : Body

    override fun create() {
        mundo = Mundo(crearMundo(ancho,alto))
        personaje = mundo.addBody(60f, 320f, 60f, 90f)

        per2 = mundo.addBody(180f,320f,60f,90f)
        per2.solid = false
        personaje.solid = true
        shapeRenderer = ShapeRenderer()
        camera = OrthographicCamera()
        viewport = ExtendViewport(1280f,720f,camera)

        viewport.apply()
        camera.position.set(ancho/2f,alto/2f,0f)


        super.create()


        mundo.addBody(80f,444f,80f,40f).also {
            it.solid = true
            it.gravedad = false
        }

       // mundo.addBody(Entidad(300f, 400f, 50f, 50f))
    }

    override fun render() {
        camera.update()
        shapeRenderer.projectionMatrix = camera.combined
        if(Gdx.input.justTouched()){
            val tp = vec3()
            tp.set(Gdx.input.x.toFloat(),Gdx.input.y.toFloat(),0f)
            camera.unproject(tp)
            println("x${tp.x.toInt()/32}  y${(tp.y.toInt())/32}")
            //getTile(Gdx.input.x,Gdx.input.y)
        }
        clearScreen(0f, 0f, 0f, 0f)
        mundo.dibujar()
        mundo.update(Gdx.graphics.deltaTime)
        when {
            Gdx.input.isKeyPressed(Input.Keys.A) -> {
                personaje.velocidad.x = -300f
            }
            Gdx.input.isKeyPressed(Input.Keys.D) -> {
                personaje.velocidad.x = 300f
            }

            else -> {
                personaje.velocidad.x =0f
            }
        }
        when{
            Gdx.input.isKeyPressed(Input.Keys.LEFT) -> {
                per2.velocidad.x = -300f
            }
            Gdx.input.isKeyPressed(Input.Keys.RIGHT) -> {
                per2.velocidad.x = 300f
            }

            else -> {
                per2.velocidad.x = 0f
            }

        }
        if(  Gdx.input.isKeyJustPressed(Input.Keys.F) ) {
            mundo.addBody(personaje.posicionx + personaje.tamanioX + 2f, personaje.posiciony + personaje.tamanioY / 2, 50f, 30f)
                    .also {
                        it.gravedad = true
                        it.velocidad.x = 500f
                    }
        }
        if(personaje.isGrounded){
            salto = 0
        }
        if(Gdx.input.isKeyJustPressed(Input.Keys.W)) {
            if (personaje.isGrounded || salto < 2){
                personaje.velocidad.y = 500f
                salto++
             }

        }else  if(Gdx.input.isKeyPressed(Input.Keys.S)){
            personaje.velocidad.y = -300f
        }
        if(Gdx.input.isKeyJustPressed(Input.Keys.UP)) {
            if (per2.isGrounded){
                per2.velocidad.y = 500f
            }

        }else  if(Gdx.input.isKeyPressed(Input.Keys.S)){
            personaje.velocidad.y = -300f
        }

    }

    override fun resize(width: Int, height: Int) {
        super.resize(width, height)
        viewport.update(width,height)
    }



}