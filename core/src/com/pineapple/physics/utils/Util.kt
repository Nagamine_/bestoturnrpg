package com.pineapple.physics.utils


import com.pineapple.physics.core.Mundo.Configuracion.TAMANIO_TILE
import ktx.collections.GdxArray
import ktx.collections.gdxArrayOf
import com.pineapple.physics.core.Tile
import kotlin.math.ceil

fun crearMundo(ancho: Int, alto: Int):GdxArray<GdxArray<Tile>> {
    val tiles = gdxArrayOf<GdxArray<Tile>>()
    for (i in 0..ancho) {
        tiles.add(gdxArrayOf(initialCapacity = ancho))
        for (j in 0..alto) {
            tiles[i].add(Tile(i, j).also {
                //vuelve solido los bordes del mundo
                if (i == 0) {
                    it.solid = true
                } else if (i == ancho) {
                    it.solid = true
                }

                if (j == 0) {
                    it.solid = true
                } else if (j == alto) {
                    it.solid = true
                }

            })
        }
    }
    return tiles
}