package com.pineapple.physics.core

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.utils.Pool
import ktx.collections.gdxArrayOf
import ktx.math.vec2

class Body() : Pool.Poolable {
    //No olvidar resetear los parametros en reset
    var posicionx: Float = 0f
    var posiciony: Float = 0f
    var tamanioX: Float = 1f
        set(value) {
            if (value > 0) {
                field = value
            } else {
                Gdx.app.error("Tamaño Entidad", "El tamaño no puede ser menor o igual a 0")
                field = 1f
            }
        }

    var tamanioY: Float = 1f
        set(value) {
            if (value > 0) {
                field = value
            } else {
                Gdx.app.error("Tamaño Entidad", "El tamaño no puede ser menor o igual a 0")
                field = 1f
            }
        }
    var velocidad = vec2(0f, 0f)
    var aceleracion = vec2(0f, 0f)
    var gravedad = true
    var solid = true

    /**Array que guarda los body con los que esta colisionando*/
    var colisionElements = gdxArrayOf<Body>()

    /**Array que guarda las funciones que se ejecutaran al iniciar una colision*/
    private var ejecutarInicio = gdxArrayOf<(body: Body) -> Unit>()

    /**Array que guarda las funciones que se ejecutaran al finalizar una colision*/
    private var ejecutarFinal = gdxArrayOf<(body: Body) -> Unit>()
    /**Array que guarda las funciones que se ejecutaran al colisionar con un tile solido*/
    private var ejecutarTile = gdxArrayOf<() -> Unit>()
    var isGrounded = false
    var continuousCol = true
    var userData: Any? = null

    constructor(posicionX: Float, posicionY: Float, tamanioX: Float, tamanioY: Float) : this() {
        set(posicionX, posicionY, tamanioX, tamanioY)
    }

    fun set(posicionx: Float, posiciony: Float, tamanioX: Float, tamanioY: Float) {
        this.posicionx = posicionx
        this.posiciony = posiciony
        this.tamanioX = tamanioX
        this.tamanioY = tamanioY
    }

    /**
     * ejecuta las funciones almacenadas en [ejecutarInicio]
     */
    fun startColisions(body: Body) {
        for (lambda in ejecutarInicio) {
            lambda(body)
        }
    }

    /**
     * ejecuta las funciones almacenadas en [ejecutarFinal]
     */
    fun endColisions(body: Body) {
        for (lambda in ejecutarFinal) {
            lambda(body)
        }
    }

    /**
     * ejecuta las funciones almacenadas en [ejecutarTile]
     */
    fun colisionTile() {
        for (lambda in ejecutarTile) {
            lambda()
        }
    }

    fun addColisionStartAction(lambda: (body: Body) -> Unit) {
        ejecutarInicio.add(lambda)
    }

    fun addColisionEndAction(lambda: (body: Body) -> Unit) {
        ejecutarFinal.add(lambda)
    }

    fun addColisionTile(lambda: () -> Unit) {
        ejecutarTile.add(lambda)
    }


    override fun reset() {
        posicionx = 0f
        posiciony = 0f
        tamanioX = 1f
        tamanioY = 1f
        velocidad.set(0f, 0f)
        aceleracion.set(0f, 0f)
        gravedad = true
        solid = true
        colisionElements.clear()
        ejecutarInicio.clear()
        ejecutarFinal.clear()
        ejecutarTile.clear()
        isGrounded = false
        continuousCol = true
        userData = null
    }

    override fun toString(): String {
        return "$posicionx $posiciony $tamanioX $tamanioY"
    }


}