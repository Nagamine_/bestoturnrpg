package com.pineapple.physics.core

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.math.Matrix4
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.utils.Pool
import ktx.collections.GdxArray
import ktx.collections.gdxArrayOf
import ktx.collections.gdxMapOf
import ktx.collections.gdxSetOf
import ktx.graphics.use
import ktx.math.vec2

import kotlin.math.absoluteValue
import kotlin.math.ceil

//BUGS vibra al estar un cuerpo sobre otro, parchado con !personaje.isgrounded
//POSIBLE BUG:
//si el elemento que hace contacto ya no esta en el cuadrante, no se eliminara de la lista de colisiones
//posible memory leak al crear un nuevo array cada que se añade un personaje
//si un body es no solido temporalmente y toca un tile, sera no solido por siempre


//notas de rendimiento:
//corregido,era un error en el bucle, pero los numeros aplican si hay esa cantidad de objetos en movimiento
//sin colision constante:
//en 130 objetos el rendimiento baja a 30fps en 180 el baja a 20fps en 235 10fpd 260 5fps 270 injugable
//2do
//en 125 objetos el rendimiento baja a 30fps en 193 el baja a 20fps en 200 10fpd 270 injugable
//con colision constante:
//en 100 objetos el rendimiento baja a 30fps en 147 el baja a 20fps en 180 10fpd  250 injugable

//cosas que se pueden añadir:
//Respuesta a colisiones(empuje,inercia,estado dinamico y estatico, velocidad relativa, objetos estaticos y dinamicos,etc)
//Movimiento conjunto(mover la entidad que se encuentra sobre el body)
//Concepto de friccion
//plataformas-objetos one way
//Formas no cuadradas?
//Rebote
/**
 * Clase donde se simulan las fisicas
 * Las entidades se deben de añadir mediante el metodo addBody y se remueven con el metodo removeBody
 */
class Mundo(var mapa: GdxArray<GdxArray<Tile>>) {
    companion object Configuracion {
        var TAMANIO_TILE = 32

        var TAMANIO_CUADRANTE = 16
    }

    /**Valor de la gravedad, puede ser vertical u horizontal
     *  valor por defecto (x:0f , y:-1200f)
     */
    var gravedad = vec2(0f, -1200f)

    /**para dibujar las entidades con shaperenderer*/
    var debugDraw = true

    /**Cantidad de tiles de ancho*/
    val anchoMundo: Int
        get() = mapa.size

    /**Cantidad de tiles de alto*/
    val altoMundo: Int
        get() = if (mapa.size > 0) mapa[0].size else 0

    /**Array que contiene todas las [Body] del mundo
     *Solo modificable con: [addBody] y [removeBody]
     */
    private val entidades: GdxArray<Body> = gdxArrayOf(ordered = false)


    /**Marca las entidades que son solidas temporalmente, vease: [desmarcarSolid]*/
    private var markedForSolid = gdxSetOf<Body>()

    /**Guarda las entidades seran removidas al final del ciclo, vease: [scheludedRemoveBody]*/
    private var remove = gdxArrayOf<Body>()

    /**Guarda los [Cuadrante]*/
    private val cuadrante: GdxArray<GdxArray<Cuadrante>> = gdxArrayOf()

    /**Sirve para verificar que una entidad no es procesada 2 veces, ver:[checkFuturaColision]*/
    private val entidadesFuturaCol = gdxSetOf<Body>()

    private val colisionNoBordes = gdxMapOf<Body, GdxArray<Body>>()

    /**Map que contiene los [Cuadrante] que hay en cada [Body] */
    private val entidadCuadrante = gdxMapOf<Body, GdxArray<Cuadrante>>()

    /**Shaperenderer para dibujar el mapa*/
    private val shapeRenderer = ShapeRenderer()

    /**pool donde estan los rectangulos para comparar bordes*/
    private var poolRectangle = object : Pool<Rectangle>(2) {
        override fun newObject(): Rectangle {
            return Rectangle()
        }

        override fun reset(o: Rectangle?) {
            super.reset(o)
            o?.set(0f, 0f, 0f, 0f)
        }
    }

    /**pool donde estan las entidades*/
    private var poolEntidad = object : Pool<Body>() {
        override fun newObject(): Body {
            return Body()
        }
    }

    /**
     * Funcion que obtiene 2 rectangulos del pool y luego de ejecutar el lambda los retorna a la pool
     * Para no olvidar de devolver al pool xd
     */
    private fun Pool<Rectangle>.useRectangles(lambda: (r1: Rectangle, r2: Rectangle) -> Unit) {
        val r1 = obtain()
        val r2 = obtain()
        lambda(r1, r2)
        free(r1)
        free(r2)

    }


    init {

        val a = ceil(anchoMundo / TAMANIO_CUADRANTE * 1.0).toInt()
        val b = ceil(altoMundo / TAMANIO_CUADRANTE * 1.0).toInt()
        for (i in 0..a) {
            cuadrante.add(gdxArrayOf(initialCapacity = b))
            for (j in 0..b) {
                cuadrante[i].add(Cuadrante(i, j))
            }
        }

    }

    /**
     * Añade una [Body] a la simulacion
     * Nota: si aparece la entidad dentro de otra, esta sera no solida temporalmente hasta que deje de colisonar
     */
    fun addBody(x: Float, y: Float, ancho: Float, alto: Float): Body {
        //añade a la lista de entidades
        val entidad = poolEntidad.obtain()
        entidad.set(x, y, ancho, alto)
        entidades.add(entidad)
        //añade el cuadrante
        entidadCuadrante.put(entidad, gdxArrayOf())
        colisionNoBordes.put(entidad, gdxArrayOf()) //posible memory leak por crear muchos array sin pool
        //actualiza los cuadrantes
        updateCuadrante(entidad)
        //si es solido y aparece colisionando se vuelve no solido hasta que la colision se resuelva
        if (entidad.solid) {
            if (checkColisionOnlySolid(entidad, 0f, 0f)) {
                entidad.solid = false
                markedForSolid.add(entidad)
            }
        }
        return entidad

    }

    fun removeBody(body: Body) {
        if (!remove.contains(body)) {
            remove.add(body)
        }

    }

    private fun scheludedRemoveBody(body: Body) {

        for (i in entidadCuadrante[body]) {
            i.entidades.removeValue(body, true)
        }
        entidadCuadrante.remove(body)

        entidades.removeValue(body, true)
        colisionNoBordes.remove(body) //posible memory leak por crear muchos array sin pool
        remove.removeValue(body, true)
        poolEntidad.free(body)
    }

    fun update(delta: Float) {
        mover(delta)
        colision()
        for (i in remove) {
            scheludedRemoveBody(i)
        }
    }

    /**
     * Funcion que se debe llamar en el loop principal, dibuja los objetos y tiles en el mundo
     * @param[projectionMatrix4] se coloca la proyeccion de la camara
     */
    fun dibujar(projectionMatrix4: Matrix4 = shapeRenderer.projectionMatrix) {
        shapeRenderer.projectionMatrix = projectionMatrix4
        if (debugDraw) {
            shapeRenderer.use(ShapeRenderer.ShapeType.Line) {
                shapeRenderer.color = Color.WHITE
                for (t in mapa) {
                    for (r in t) {
                        if (r.solid) {
                            shapeRenderer.color = Color.GREEN
                        } else {
                            shapeRenderer.color = Color.BLACK
                        }

                        shapeRenderer.rect(r.coordenadaX * TAMANIO_TILE.toFloat(), r.coordenadaY * TAMANIO_TILE.toFloat(), TAMANIO_TILE.toFloat() - 1, TAMANIO_TILE.toFloat() - 1)
                    }

                }
                for (entidad in entidades) {
                    shapeRenderer.color = Color.BLUE
                    if (!entidad.solid) {
                        shapeRenderer.color = Color.CYAN
                    }
                    shapeRenderer.rect(entidad.posicionx, entidad.posiciony, entidad.tamanioX, entidad.tamanioY)
                }

                //  shapeRenderer.rect(personaje.posx,personaje.posy,32f*2,32f*3)
            }
        }
    }

    /**
     * Simulacion de las fisicas:
     * 1) Se iteran todas las entidades en el mundo
     * 2) Se actualiza la velocidad y posicion
     * 3) Se itera el movimiento paso por paso ( movimiento siempre menor a 1 tile)
     * 4) Se obtiene el tile de donde estara el objeto en el siguiente frame
     * 5) Se repite por cada tile en el objeto del lado que se desea detectar (izquierda, derecha, arriba,abajo)
     * //si el personaje mide 4 tiles de alto, habran 5 iteraciones para la colision izquierda y 5 para la derecha del (0,0) al (0,4)
     * 6) Si el futuro tile es solido, la posicion del objeto se vuelve la del borde del tile
     * 7) sino, se verifica si hay alguna colision con otra entidad en el lugar donde se deberia mover
     * 8) si la hay y es un objeto solido, se obtiene el objeto mas cercano entre todas las colisiones
     * 9) se actualiza el objeto para colocarse al costado del objeto colisionado
     * 10) en caso no hayan colisiones se mueve el personaje
     * 11)1repetir los pasos 4 al 10 por cada direccion (izquierda, derecha, arriba,abajo)
     *
     * NOTA:
     * hay 3 while anidados, en teoria complejidad O(n^3),
     * sin embargo en la practica:
     * 1er while:  complejidad O(n) : Iteracion sobre todas las entidades
     * 2do while: complejidad en velocidades menores a tamañoTile*60 (32*60)=1800 O(1)
     * 3er while: complejidad en personaje tamaño =O(n) donde n es tamaño del objeto
     * ejem:2tiles * 3 tiles = O(3) por lado
     * 3er while: por cada entidad moviendose se realiza una comparacion con todos los objetos en el cuadrante
     *
     */
    private fun mover(deltatime: Float) {
        //1) Se iteran todas las entidades en el mundo
        for (personaje in entidades) {
            //2) Se actualiza la velocidad y posicion
            if (personaje.gravedad && !personaje.isGrounded) {
                personaje.velocidad.x = personaje.velocidad.x + (personaje.aceleracion.x + gravedad.x) * deltatime
                personaje.velocidad.y = personaje.velocidad.y + (personaje.aceleracion.y + gravedad.y) * deltatime
            } else {
                personaje.velocidad.x = personaje.velocidad.x + personaje.aceleracion.x * deltatime
                personaje.velocidad.y = personaje.velocidad.y + personaje.aceleracion.y * deltatime
            }
            val movimientoxTotal = personaje.velocidad.x * deltatime
            val movimientoyTotal = personaje.velocidad.y * deltatime

            //movimiento por paso

            var movimientox = TAMANIO_TILE - 2f
            var movimientoy = TAMANIO_TILE - 2f
            if (personaje.tamanioX < TAMANIO_TILE - 2f) {
                movimientox = if (personaje.tamanioX <= 1f && !personaje.continuousCol) 1f else personaje.tamanioX
            }

            if (personaje.tamanioY < TAMANIO_TILE - 2f) {
                movimientoy = if (personaje.tamanioY <= 1f && !personaje.continuousCol) 1f else personaje.tamanioY
            }


            //acumulador de movimiento por paso
            var movimientoPasox = 0f
            var movimientoPasoy = 0f

            //3) Se itera el movimiento paso por paso
            movimientoLateral@ while (movimientoPasox < movimientoxTotal.absoluteValue) {
                updateCuadrante(personaje)
                //si el movimiento es menor al mov maximo entonces el mov max es el movimiento
                if (movimientoxTotal.absoluteValue < movimientox) {
                    movimientox = movimientoxTotal.absoluteValue
                }
                //eliminacion de exceso en la ultima iteracion
                if (movimientoxTotal.absoluteValue < movimientox + movimientoPasox) {
                    movimientox = movimientoxTotal.absoluteValue - movimientoPasox
                }
                //movimiento a la DERECHA
                if (movimientoxTotal > 0) {

                    //mueve a la derecha y si colisiona, sale del while
                    if (moverDerecha(personaje, movimientox)) {
                        break@movimientoLateral
                    }
                    //movimiento a la IZQUIERDA
                } else if (movimientoxTotal < 0) {
                    if (moverIzquierda(personaje, movimientox)) {
                        break@movimientoLateral
                    }
                }
                movimientoPasox += movimientox
            }// fin while (movimientoPasox < movimientoxTotal.absoluteValue)

            movimientoVertical@ while (movimientoPasoy < movimientoyTotal.absoluteValue) {
                updateCuadrante(personaje)
                if (movimientoyTotal.absoluteValue < movimientoy) {
                    movimientoy = movimientoyTotal.absoluteValue
                }
                if (movimientoyTotal.absoluteValue < movimientoy + movimientoPasoy) {
                    movimientoy = movimientoyTotal.absoluteValue - movimientoPasoy
                }
                //movimiento hacia ARRIBA
                if (movimientoyTotal > 0) {
                    if (moverArriba(personaje, movimientoy)) {
                        break@movimientoVertical
                    }
                } else if (movimientoyTotal < 0) {
                    if (moverAbajo(personaje, movimientoy)) {
                        break@movimientoVertical
                    }
                }
                movimientoPasoy += movimientoy
            }

            isGrounded(personaje)

            updateCuadrante(personaje)
            desmarcarSolid(personaje)
        }

    }

    /**
     * verifica si la entidad esta en el aire
     */
    private fun isGrounded(personaje: Body) {
        val izquierda = getTile(personaje.posicionx+1, personaje.posiciony - 1)
        val derecha = getTile(personaje.posicionx + personaje.tamanioX -1, personaje.posiciony - 1)
        if (izquierda != null && derecha != null) {
            personaje.isGrounded = izquierda.solid || derecha.solid
        } else {
            removeBody(personaje)
        }
        if (!personaje.isGrounded && personaje.solid) {
            if (checkColisionOnlySolid(personaje, 0f, -1.1f)) {
                personaje.isGrounded = true
            }
        }

    }

    private fun moverDerecha(personaje: Body, movimientox: Float): Boolean {


        var tam = 0f
        var colision = false
        //4) Se obtiene el tile de donde estara el objeto en el siguiente frame
        //5) Se repite por cada tile en el objeto del lado que se desea detectar (izquierda, derecha, arriba,abajo)
        //si el personaje mide 4 tiles de alto, habran 5 iteraciones para la colision izquierda y 5 para la derecha
        //del (0,0) al (0,4)

        //verifica colisiones con otros body


        if (personaje.solid) {
            //7) sino, se verifica si hay alguna colision con otra entidad en el lugar donde se deberia mover
            if (checkFuturaColision(personaje, movimientox, 0f)) {
                //8) si la hay y es un objeto solido, se obtiene el objeto mas cercano entre todas las colisiones
                var minDer = Float.MAX_VALUE
                for (p in colisionNoBordes[personaje]) {
                    if (p.solid) {
                        if (p.posicionx < minDer) {
                            minDer = p.posicionx
                        }
                    }
                }
                //9) se actualiza el objeto para colocarse al costado del objeto colisionado
                if (minDer != Float.MAX_VALUE) {

                    personaje.posicionx = minDer - personaje.tamanioX
                    personaje.velocidad.x = 0f
                    colision = true
                }
            }
        }
        if (!colision) {
            sensorsX@ while (tam <= personaje.tamanioY) {
                val sensorDerecha = getTile(personaje.posicionx + movimientox + personaje.tamanioX, personaje.posiciony + tam)

                if (sensorDerecha != null) {

                    //6) Si el futuro tile es solido, la posicion del objeto se vuelve la del tile
                    if (sensorDerecha.solid) {
                        if (colisionTile(personaje, movimientox, 0f, sensorDerecha)) {
                            personaje.colisionTile()
                            colision = true
                            personaje.posicionx = sensorDerecha.coordenadaX * TAMANIO_TILE - personaje.tamanioX
                            personaje.velocidad.x = 0f
                            break@sensorsX
                        }
                    }
                } else {
                    removeBody(personaje)
                }
                if (personaje.tamanioY == tam) {
                    tam = personaje.tamanioY + 1 // puede ser reemplazado con break
                } else {
                    if (personaje.tamanioY < TAMANIO_TILE + tam) {
                        tam = personaje.tamanioY
                    } else {
                        tam += TAMANIO_TILE
                    }
                }
            }


        }
        if (!colision) {
            //10) en caso no hayan colisiones se mueve el personaje
            personaje.posicionx += movimientox
        }
        return colision
    }

    private fun moverIzquierda(personaje: Body, movimientox: Float): Boolean {

        var tam = 0f
        var colision = false
        if (personaje.solid) {
            if (checkFuturaColision(personaje, -movimientox, 0f)) {
                var maxIzq = Float.MIN_VALUE
                var tama = 0f
                for (p in colisionNoBordes[personaje]) {
                    if (p.solid) {
                        if (p.posicionx + p.tamanioX > maxIzq) {
                            maxIzq = p.posicionx
                            tama = p.tamanioX
                        }
                    }
                }
                if (maxIzq != Float.MIN_VALUE) {
                    personaje.posicionx = maxIzq + tama
                    personaje.velocidad.x = 0f
                    colision = true
                }
            }
        }
        if (!colision) {
            sensorsY@ while (tam <= personaje.tamanioY) {
                val sensorIzquierda = getTile(personaje.posicionx - movimientox, personaje.posiciony + tam)
                if (sensorIzquierda != null) {

                    if (sensorIzquierda.solid) {
                        if (colisionTile(personaje, -movimientox, 0f, sensorIzquierda)) {
                            personaje.colisionTile()
                            colision = true
                            personaje.posicionx = (sensorIzquierda.coordenadaX * TAMANIO_TILE + TAMANIO_TILE).toFloat()
                            personaje.velocidad.x = 0f
                            break@sensorsY
                        }
                    }
                } else {
                    removeBody(personaje)
                }
                if (personaje.tamanioY == tam) {
                    tam = personaje.tamanioY + 1 // puede ser reemplazado con break
                } else {
                    if (personaje.tamanioY < TAMANIO_TILE + tam) {
                        tam = personaje.tamanioY
                    } else {
                        tam += TAMANIO_TILE
                    }
                }
            }

        }
        if (!colision) {
            personaje.posicionx -= movimientox
        }
        return colision

    }

    private fun moverArriba(personaje: Body, movimientoy: Float): Boolean {

        var tam = 0f
        var colision = false


        if (personaje.solid) {
            if (checkFuturaColision(personaje, 0f, movimientoy)) {
                var minArr = Float.MAX_VALUE
                for (p in colisionNoBordes[personaje]) {
                    if (p.solid) {
                        if (p.posiciony + p.tamanioY < minArr) {
                            minArr = p.posiciony
                        }
                    }
                }
                if (minArr != Float.MAX_VALUE) {
                    personaje.posiciony = minArr - personaje.tamanioY
                    personaje.velocidad.y = 0f

                    colision = true
                }
            }
        }
        if (!colision) {

            sensorsY@ while (tam <= personaje.tamanioX) {
                val sensorArriba = getTile(personaje.posicionx + tam, personaje.posiciony + movimientoy + personaje.tamanioY)
                if (sensorArriba != null) {

                    if (sensorArriba.solid) {
                        if (colisionTile(personaje, 0f, movimientoy, sensorArriba)) {
                            personaje.colisionTile()

                            personaje.posiciony = sensorArriba.coordenadaY * TAMANIO_TILE - personaje.tamanioY
                            personaje.velocidad.y = 0f
                            colision = true
                            break@sensorsY
                        }
                    }
                } else {
                    removeBody(personaje)
                }
                if (personaje.tamanioX == tam) {
                    tam = personaje.tamanioX + 1 // puede ser reemplazado con break
                } else {
                    if (personaje.tamanioX < TAMANIO_TILE + tam) {
                        tam = personaje.tamanioX
                    } else {
                        tam += TAMANIO_TILE
                    }
                }
            }


        }
        if (!colision) {
            personaje.posiciony += movimientoy
        }
        return colision
    }

    //BUG vibra al estar un cuerpo sobre otro, parchado con !personaje.isgrounded
    private fun moverAbajo(personaje: Body, movimientoy: Float): Boolean {
        if (!personaje.isGrounded) {
            var tam = 0f
            var colision = false
            if (personaje.solid) {
                if (checkFuturaColision(personaje, 0f, -movimientoy)) {
                    var maxAbajo = Float.MIN_VALUE
                    var tama = 0f
                    for (p in colisionNoBordes[personaje]) {
                        if (p.solid) {
                            if (p.posiciony + p.tamanioY > maxAbajo) {
                                maxAbajo = p.posiciony
                                tama = p.tamanioY
                            }
                        }
                    }
                    if (maxAbajo != Float.MIN_VALUE) {
                        personaje.posiciony = maxAbajo + tama
                        personaje.velocidad.y = 0f

                        colision = true
                    }
                }
            }
            if (!colision) {

                sensorsY@ while (tam <= personaje.tamanioX) {
                    val sensorAbajo = getTile(personaje.posicionx + tam, personaje.posiciony - movimientoy)
                    if (sensorAbajo != null) {


                        if (sensorAbajo.solid) {
                            if (colisionTile(personaje, 0f, -movimientoy, sensorAbajo)) {
                                personaje.colisionTile()
                                colision = true
                                personaje.posiciony = (sensorAbajo.coordenadaY * TAMANIO_TILE + TAMANIO_TILE).toFloat()
                                personaje.velocidad.y = 0f
                                break@sensorsY
                            }
                        }
                    } else {
                        removeBody(personaje)
                    }

                    if (personaje.tamanioX == tam) {
                        tam = personaje.tamanioX + 1 // puede ser reemplazado con break
                    } else {
                        if (personaje.tamanioX < TAMANIO_TILE + tam) {
                            tam = personaje.tamanioX
                        } else {
                            tam += TAMANIO_TILE
                        }
                    }
                }

            }
            if (!colision) {
                personaje.posiciony -= movimientoy
            }
            return colision
        }
        return true
    }

    /**
     * Actualiza los cuadrantes para saber que unidad iterar
     * 1) obtiene todos los cuadrantes donde esta ubicada la entidad
     * 2)recorre todos los cuadrantes anteriores donde estaba ubicada la entidad
     * 3)Si el cuadrante no se encuentra ahi, se procede a retirar el cuadrante de la lista
     * 4)se añaden los nuevos cuadrantes que no estaban en la lista
     */
    private fun desmarcarSolid(body: Body) {
        if(markedForSolid.contains(body)) {
            if (!checkColisionOnlySolid(body, 0f, 0f)) {
                body.solid = true
                markedForSolid.remove(body)
            }
        }
    }

    private fun updateCuadrante(body: Body) {

        val i = getCuadrantes(body)

        for (c in entidadCuadrante[body]) {
            if (!i.contains(c)) {
                entidadCuadrante[body].removeValue(c, true)
                c.entidades.removeValue(body, true)
            }
        }
        for (cuad in i) {
            if (cuad != null) {
                if (!entidadCuadrante[body].contains(cuad)) {
                    cuad.entidades.add(body)
                    entidadCuadrante[body].add(cuad)

                }
            } else {
                removeBody(body)
            }
        }

    }

    /**obtiene el tile ubicado en esas coordenadas
     *
     */
    fun getTile(x: Float, y: Float): Tile? {
        val x1 = x.toInt() / TAMANIO_TILE
        val y1 = y.toInt() / TAMANIO_TILE
        if (x1 < mapa.size && y1 < mapa[0].size && x1 >= 0 && y1 >= 0) {
            return mapa[x1][y1]
        }

        Gdx.app.error("TILES", "Tile fuera de rango")
        return null
    }

    private fun colisionTile(body: Body, movimientox: Float, movimientoy: Float, tile: Tile): Boolean {
        var a = false
        poolRectangle.useRectangles { r1, r2 ->
            r1.set(body.posicionx + movimientox, body.posiciony + movimientoy, body.tamanioX, body.tamanioY)
            r2.set(tile.coordenadaX * TAMANIO_TILE.toFloat(), tile.coordenadaY * TAMANIO_TILE.toFloat(), TAMANIO_TILE.toFloat(), TAMANIO_TILE.toFloat())
            a = r1.overlaps(r2)
        }
        return a
    }

    /**obtiene el cuadrante en esas coordenadas
     *
     */
    private fun getCuadrante(x: Float, y: Float): Cuadrante? {
        val x1 = x.toInt() / TAMANIO_TILE / TAMANIO_CUADRANTE
        val y1 = y.toInt() / TAMANIO_TILE / TAMANIO_CUADRANTE
        if (x1 < cuadrante.size && y1 < cuadrante[0].size) {
            return cuadrante[x1][y1]
        }

        Gdx.app.error("CUADRANTES", "Cuadrante fuera de rango")
        return null
    }

    /**
     * Obtiene los cuadrantes donde esta ubicado actualmente el objeto
     */
    private fun getCuadrantes(body: Body): GdxArray<Cuadrante> {
        val g = gdxArrayOf<Cuadrante>()
        val inferiorIzquierda = getCuadrante(body.posicionx, body.posiciony)
        val inferiorDerecha = getCuadrante(body.posicionx + body.tamanioX, body.posiciony)
        val superiorIzquierda = getCuadrante(body.posicionx, body.posiciony + body.tamanioY)
        val superiorDerecha = getCuadrante(body.posicionx + body.tamanioX, body.posiciony + body.tamanioY)
        val centro = getCuadrante(body.posicionx + body.tamanioX / 2, body.posiciony + body.tamanioY / 2)
        g.add(inferiorIzquierda)
        if (!g.contains(inferiorDerecha)) {
            g.add(inferiorDerecha)
        }
        if (!g.contains(superiorIzquierda)) {
            g.add(superiorIzquierda)
        }
        if (!g.contains(superiorDerecha)) {
            g.add(superiorDerecha)
        }
        if (!g.contains(centro)) {
            g.add(centro)
        }
        return g

    }

    private fun checkColisionOnlySolid(body: Body, movimientox: Float, movimientoy: Float): Boolean {
        var g = false
        if (checkFuturaColision(body, movimientox, movimientoy)) {
            for (i in colisionNoBordes[body]) {
                if (i.solid) {
                    g = true
                }
            }

        }
        return g
    }

    private fun checkFuturaColision(body: Body, movimientox: Float, movimientoy: Float): Boolean {
        var existColision = false
        entidadesFuturaCol.clear()

        poolRectangle.useRectangles { r1, r2 ->
            r1.set(body.posicionx + movimientox, body.posiciony + movimientoy, body.tamanioX, body.tamanioY)
            for (cuadrante in entidadCuadrante[body]) {
                for (e in cuadrante.entidades) {
                    if (!(e === body)) {
                        if (!entidadesFuturaCol.contains(e)) {
                            entidadesFuturaCol.add(e)
                            r2.set(e.posicionx, e.posiciony, e.tamanioX, e.tamanioY)
                            val col = r1.overlaps(r2)
                            if (!colisionNoBordes[e].contains(body)) {
                                if (col) {
                                    colisionNoBordes[e].add(body)
                                }
                                //4 en caso de que se encuentre y no colisione se remueve
                            } else {
                                if (!col) {
                                    colisionNoBordes[e].removeValue(body, true)
                                }
                            }
                            if (!colisionNoBordes[body].contains(e)) {
                                if (col) {
                                    colisionNoBordes[body].add(e)
                                }
                                //4 en caso de que se encuentre y no colisione se remueve
                            } else {
                                if (!col) {
                                    colisionNoBordes[body].removeValue(e, true)
                                }
                            }
                            if (col) {

                                existColision = true
                            }
                        }
                    }

                }
            }
        }


        return existColision


    }

    /**
     * Por cada entidad verifica cada uno de los cuadrantes donde se encuentra y calcula la colision
     * en caso hay colision ejecuta startcontact y se añade a la lista de contactos,
     * luego revisa si ya no esta en contacto y si no esa se remueve de la lista de contactos
     */
    //POSIBLE BUG:
    //si el elemento que hace contacto ya no esta en el cuadrante, no se eliminara de la lista de colisiones
    private fun colision() {
        for (body in entidades) {
            entidadesFuturaCol.clear()
            poolRectangle.useRectangles { r1, r2 ->
                r1.set(body.posicionx -1, body.posiciony -1, body.tamanioX+2, body.tamanioY+2)
                for (cuadrante in entidadCuadrante[body]) {
                    for (e in cuadrante.entidades) {
                        if (!(e === body)) {
                            if (!entidadesFuturaCol.contains(e)) {
                                entidadesFuturaCol.add(e)
                                r2.set(e.posicionx, e.posiciony, e.tamanioX, e.tamanioY)
                                val col = r1.overlaps(r2)
                                if(e.posicionx -1 == 523.0f){
                                }
                                if (!e.colisionElements.contains(body)) {
                                    if (col) {
                                        e.colisionElements.add(body)
                                        e.startColisions(body)
                                    }
                                    //4 en caso de que se encuentre y no colisione se remueve
                                } else {
                                    if (!col) {
                                        e.colisionElements.removeValue(body, true)
                                        e.endColisions(body)
                                    }
                                }
                                if (!body.colisionElements.contains(e)) {
                                    if (col) {
                                        body.colisionElements.add(e)
                                        body.startColisions(e)
                                    }
                                    //4 en caso de que se encuentre y no colisione se remueve
                                } else {
                                    if (!col) {
                                        body.colisionElements.removeValue(e, true)
                                        body.endColisions(e)
                                    }
                                }

                            }
                        }

                    }
                }
            }
        }

    }


}